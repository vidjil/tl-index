#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include <ctype.h>
#include <unistd.h>
#include "./RopeBwt2/rld0.h"
#include "./RopeBwt2/rle.h"
#include "./RopeBwt2/mrope.h"
#include "./RopeBwt2/crlf.h"
#include "./RopeBwt2/kseq.h"

KSEQ_INIT(gzFile, gzread)

static unsigned char seq_nt6_table[128] = {
    0, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,
    5, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,
    5, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,
    5, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,
    5, 1, 5, 2,  5, 5, 5, 3,  5, 5, 5, 5,  5, 5, 5, 5,
    5, 5, 5, 5,  4, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5,
    5, 1, 5, 2,  5, 5, 5, 3,  5, 5, 5, 5,  5, 5, 5, 5,
    5, 5, 5, 5,  4, 5, 5, 5,  5, 5, 5, 5,  5, 5, 5, 5
};


// gcc bwt.c ../rle.c ../mrope.c ../rld0.c ../rope.c outils.c -o test -lpthread -lz -g && ./test out.bwt in.fa



void saveRope(char* file, mrope_t *mr){
	FILE *f;
	f = fopen(file, "w");
	if (f!= NULL){
		mr_dump(mr, f);
		fclose(f);
	} else
        printf("Save Rope : Impossible d'ouvrir le fichier en écriture");
}


mrope_t* restoreRope(char* file){
	mrope_t *mr;
	FILE *f;
	f= fopen(file, "r");
	if (f!= NULL){
		mr = mr_restore(f);
		fclose(f);
	} else
        printf("Save Rope : Impossible d'ouvrir le fichier en lecture");
	return mr;
}

long getSizeRope(char* file, mrope_t *mr){
	saveRope(file, mr);
	FILE* monFichier = fopen(file, "r");
	long taille = 0;
	if(monFichier!=NULL){
	    fseek(monFichier,0,SEEK_END);
    	taille = ftell(monFichier);
	    fclose(monFichier);
	}
	return taille;
}






static inline int kputsn(const char *p, int l, kstring_t *s)
{
	if (s->l + l + 1 >= s->m) {
		char *tmp;
		s->m = s->l + l + 2;
		kroundup32(s->m);
		if ((tmp = (char*)realloc(s->s, s->m))) s->s = tmp;
		else return EOF;
	}
	memcpy(s->s + s->l, p, l);
	s->l += l;
	s->s[s->l] = 0;
	return l;
}


int makeBwt(mrope_t* mr, char* inFile){

	//mrope_t *mr = 0;
	gzFile fp;
	kseq_t *ks;
	int i;
	kstring_t buf = { 0, 0, 0 };
	
	freopen(optarg, "w", stdout); 
	//mr = mr_init(64, 512, 0);
	
	fp = gzopen(inFile, "rb");
	ks = kseq_init(fp);
	for (;;) {
		int l;
		uint8_t *s;
		if (kseq_read(ks) < 0){
			break; // read fasta/fastq
		}
		l = ks->seq.l;
		s = (uint8_t*)ks->seq.s;
		for (i = 0; i < l; ++i) // change encoding
			s[i] = s[i] < 128? seq_nt6_table[s[i]] : 5;
		
		for (i = 0; i < l>>1; ++i) { // reverse
			int tmp = s[l-1-i];
			s[l-1-i] = s[i]; s[i] = tmp;
		}
		kputsn((char*)ks->seq.s, ks->seq.l + 1, &buf);
	}

	mr_insert_multi(mr, buf.l, (uint8_t*)buf.s, 0);
	
	return 0;

}


mrope_t* bwt(const char* fichier){
	
	mrope_t *mr = 0;
	mr = mr_init(64, 512, 0);

	makeBwt(mr, fichier);

	return mr;
}



// tester ce que veut dire : 
//for (j = 0; j < l; ++j) putchar("$ACGTN"[c]);

//finir la redirection de sortie
