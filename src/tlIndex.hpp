#ifndef TLINDEX_H
#define TLINDEX_H


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <sdsl/wavelet_trees.hpp>

#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include "annotation.hpp"
#include "wtAnno.hpp"
#include "tlbwt.hpp"
#include "tlbwt.hpp"

#include "index.hpp"

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

using namespace std;
using namespace sdsl;

class TLIndex : public Index{

	private:
		WTAnno *wt;
		rrr_vector<127> seq;
		rrr_vector<127>::rank_1_type s_rank;
		rrr_vector<127>::select_1_type s_sel;
		
		// Put the right label in the vecteur at position pos, then look for the following position in the BWT
		int changeLettreBwtRope(int pos, uint16_t code, vector<uint16_t> &tags);
		/* For every sequence, we made a vector of annotation (labels with their begining and end).
		We made a vector of labels (for all the labels of the file).
		At the same time, a map save the occurrences of the label and their code (travel in the VDJ tree)*/
		void lettreToTagRope(ArbreVDJ* arb, string fichierIn, vector<uint16_t> &tags, bit_vector &seq);

		void makeLink(vector<uint16_t> &tags, bit_vector &b);
		vector<uint16_t> makeRacineWt(vector<uint16_t> &tags);
		void makeWt(vector<uint16_t> &tags);
		void initWt(WTAnno *w);
		void setSeq(bit_vector l);

		vector<int> findM(string motif);
	
	public:
		TLIndex(string file, bool result);
		TLIndex(string dossier, string file, bool btime, bool result);
		~TLIndex();

	//Main queries
	string labelU(int pos);
	string label(int pos);
	vector<int> findMotif(string motif);
	vector<int> findL(string anno);
	// returns the positions which have a pattern labelled anno
	vector<int> findMotifXAnno(string motif, string anno);

	//Accessor to the sequence

	// from a position in the BWT, returns the sequence's id
	int numSeq(int pos);	
	vector<Annotation*> findAnnoFromNumber(int numero);
	vector<int> posBwtToWt(vector<int> motif);
	vector<int> posBwtToT(vector<int> pos);
	int beginSeq(int numSeq);
	pair<int, int>findNumSeq(int pos);

	//Save / Restore
	void saveAll(string file);
	void restoreIndex(string file);

	// Visualisations
	void visuWt();
	void seqOrigine();
	void printLabels() const;

	//Getters / Setters
	int nbLettresRoot() const;
	int nbAnnoDiff() const;
	string getLabel(int i) const;
	int sizeAnno() const;
	int sizeLink() const;
	int sizeHelper() const;
	string sequence(int i);


};
#endif