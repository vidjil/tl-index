#ifndef BWT_H_
#define BWT_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "./RopeBwt2/rld0.h"
#include "./RopeBwt2/rle.h"
#include "./RopeBwt2/mrope.h"
#include "./RopeBwt2/crlf.h"
#include "./RopeBwt2/kseq.h"


 
#ifdef __cplusplus
extern "C" {
#endif

void saveRope(char* file, mrope_t *mr);
mrope_t* restoreRope(char* file);
long getSizeRope(char* file, mrope_t *mr);
int makeBwt(mrope_t* mr, char* inFile);
static inline int kputsn(const char *p, int l, kstring_t *s);
mrope_t* bwt(const char* fichier);


#ifdef __cplusplus
}
#endif
 
#endif /* BWT_H_ */