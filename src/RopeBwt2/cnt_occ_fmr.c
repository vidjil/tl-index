#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "kvec.h"
#include "mrope.h"

typedef struct {
	int64_t k, l;
	int d, c;
} pair64_t;

int main(int argc, char *argv[])
{
	mrope_t *e;
	int c, a, min_occ = 100, depth = 50;
	pair64_t *p;
	kvec_t(pair64_t) stack = {0,0,0};
	char *str;

	while ((c = getopt(argc, argv, "d:o:")) >= 0) {
		if (c == 'd') depth = atol(optarg);
		else if (c == 'o') min_occ = atol(optarg);
	}
	if (optind == argc) {
		fprintf(stderr, "Usage: cnt_occ [-d depth=50] [-o minOcc=100] in.fmd\n");
		return 1;
	}

	str = calloc(1, depth + 1);
	e = mr_restore(fopen(argv[optind], "rb"));
	kv_pushp(pair64_t, stack, &p);
	for (a = 0, p->l = 0; a < 6; ++a)
		for (c = 0; c < 6; ++c)
			p->l += e->r[a]->c[c];
	p->k = 0, p->d = p->c = 0;
	while (stack.n) {
		int64_t ok[6], ol[6];
		int a;
		pair64_t top = kv_pop(stack);
		if (top.d > 0) str[top.d-1] = "$ACGTN"[top.c];
		mr_rank2a(e, top.k, top.l, ok, ol);
		for (a = 1; a <= 4; ++a) {
			if (ol[a] - ok[a] < min_occ) continue;
			str[top.d] = "$ACGTN"[a];
			if (top.d != depth - 1) {
				int64_t ac[7];
				mr_get_ac(e, ac);
				kv_pushp(pair64_t, stack, &p);
				p->k = ac[a] + ok[a];
				p->l = ac[a] + ol[a];
				p->d = top.d + 1;
				p->c = a;
			} else {
				fputs(str, stdout); fputc('\t', stdout);
				printf("%ld\n", (long)(ol[a] - ok[a]));
			}
		}
	}
	free(stack.a);
	mr_destroy(e);
	return 0;
}
