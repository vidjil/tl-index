#include "annotation.hpp"
#include <string>

using namespace std;

Annotation::Annotation(string n, int d, int f){
	this->nom = n;
	this->debut = d;
	this->fin = f;
}

string Annotation::getName() const{
	return this->nom;
}

int Annotation::getDebut() const{
	return this->debut;
}

int Annotation::getFin() const{
	return this->fin;
}

void Annotation::toString() const{
	cout << this->nom << ":" << this->debut << "-" << this->fin <<endl;
}


