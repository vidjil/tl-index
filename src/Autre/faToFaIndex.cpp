#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, char* argv[]){

	if (argc < 2){
		cout << "./faToFaIndex input.fa output.fa" << endl;
		return 0;
	}

	string in = argv[1];
	ifstream fluxLecture(in.c_str()); 
	string out = argv[2];
	ofstream fluxEcriture(out.c_str());

	if(fluxEcriture) {
    	//string ligne;
    	int taille;
    	int ecarts[10];
    	string genes[5];
    	string mot1;
    	string mot2 = "";

    	//for (int i=0; i<10; i++){
		//	fluxLecture >> mot1;
    	while(fluxLecture >> mot1){ 

			if (mot1[0] == '>'){
				if (mot2 != ""){
						fluxEcriture << '>';
					for (int i=0; i<taille; i++){
						if (genes[i][3] == 'V' || genes[i][3] == 'D' ||genes[i][3] == 'J')
						fluxEcriture << genes[i] << ":" << (ecarts[2*i]-1) << "-" << min((ecarts[2*i+1]-1), (int) mot2.size()) << " ";
					}
					fluxEcriture << endl;
					fluxEcriture << mot2 << endl;		
				}
				taille = mot1.size()-1;
				for (int i=0; i<taille*2; i++){
					fluxLecture >> ecarts[i];
				}
				for (int i=0; i<taille; i++){
					fluxLecture >> genes[i];
					fluxLecture >> mot1;
				}
			}
			mot2 = mot1;
		}
		for (int i=0; i<taille; i++){
			if (genes[i][3] == 'V' || genes[i][3] == 'D' ||genes[i][3] == 'J')
				fluxEcriture << genes[i] << ":" << (ecarts[2*i]-1) << "-" << min((ecarts[2*i+1]-1), (int)mot2.size()) << " ";
			}
		fluxEcriture << endl;
		fluxEcriture << mot2 << endl;

		cout << "===> " << argv[2] << endl;

	}
	else {
    	cout << "ERREUR: Can't open input file." << endl;
	}
	
}