#ifndef ANNOTATION_H
#define ANNOTATION_H


#include <iostream>
#include <string>

using namespace std;

class Annotation{
	
	private :
		int debut;
		int fin;
		string nom;

	public :
		Annotation(string nom, int debut, int fin);
		string getName() const;
		int getDebut() const;
		int getFin() const;
		void toString() const;

};
#endif