#include <iostream>

#include "anno.hpp"

using namespace std;
using namespace sdsl;


Anno::Anno(){
	bit_vector l;
	this->link = l;
}

Anno::Anno(string file){
	string fichierLink = file + ".link";
   	restoreLink(fichierLink);
    rrr_vector<127>::rank_1_type r(&this->link);
	this->b_rank = r;
	rrr_vector<127>::select_1_type s(&this->link);
	this->b_sel = s;
}

Anno::~Anno(){
    // this->link.clear();
}




void Anno::setLink(bit_vector l){
    rrr_vector<127> rrrl(l);
    this->link = rrrl;
    rrr_vector<127>::rank_1_type r(&this->link);
    this->b_rank = r;
    rrr_vector<127>::select_1_type s(&this->link);
    this->b_sel = s;
}


void Anno::saveLink(string fileLink){
    char byte;
    ofstream fluxEc(fileLink);  
    if(fluxEc){
        int taille = this->link.size();
        fluxEc << taille << endl;
        int t=0;
        while (t<taille){
            byte=0;
            for (int i = 0; i < 8; i++)
                byte |= (this->link[i+t] & 1) << (7-i);
            fluxEc.put(byte);
            t+=8;
        }
        fluxEc.close();
    } else
        cerr << "Error when trying to open the file (writing) : " << fileLink << endl;
}

void Anno::restoreLink(string file){
	char byte;
	ifstream fluxLec(file);
    if (fluxLec){
    	int taille;
    	fluxLec >> taille;
    	bit_vector bit = bit_vector(taille, 0);
        fluxLec.get(byte); //caractere retour à la ligne

        int i=0;
        while(fluxLec.get(byte)){
    		for (int j=0; j<8; j++){
    			bit[i+j]= (byte >> (7-j)) & 1;
    		}
            i+=8;
    	}
        rrr_vector<127> rrrl(bit);
        this->link = rrrl;
    	fluxLec.close();
    } else
    	cerr << "Error when trying to open the file (reading) : " << file << endl; 

}


void Anno::printLink() const{
    cout << this->link<< endl;
}


int Anno::sizeLink() const{
    int t =  size_in_bytes(this->link) + size_in_bytes(this->b_rank) + size_in_bytes(this->b_sel);
    return t;
}