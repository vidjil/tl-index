#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <tuple>
#include <cassert>
#include <map>
#include <vector>
#include <bitset>
#include <cstdint>
#include <ctime> 

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include "tlIndex.hpp"
#include "outils.hpp"
#include "wtAnno.hpp"
#include "visu.cpp"
#include "tlbwt.hpp"

// uint16_t TAGNULL = 65535; // max : 1111111111111111

using namespace std;
using namespace sdsl;


/****************
  Construction
****************/

TLIndex::TLIndex(string file, bool result) : Index(file, result){
	WTAnno *wt = new WTAnno(file);
	this->wt = wt;

	if (result){
		cout << "link vector : " ;
		this->wt->printLink();
		visuWt();
	}
}

TLIndex::TLIndex(string dossier, string file, bool btime, bool result) : Index(dossier, file, btime, result){
	ArbreVDJ *arb = new ArbreVDJ();
	arb->constructionArbre(dossier);
	if (btime)
		cout << "Germlines Tree	->      OK" << endl;

	time_t tend, tmid;
    double texec=0.;
    double tlink=0., twt=0.;
    tmid=time(NULL);
	WTAnno *wt = new WTAnno();
	this->wt = wt;
	vector<uint16_t> tags;
	tags.resize(this->taille, 65535); //TODO : TAGNULL
	bit_vector seq = bit_vector(taille, 0);
	lettreToTagRope(arb, file, tags, seq);
	setSeq(seq);

	if (result){
		cout << "Labels : "; 
		for (int i = 0; i<tags.size(); i++)
			cout << tags[i] << " " ;
		cout << endl;
	}

	bit_vector l = bit_vector(taille, 1);
	makeLink(tags, l);
	this->wt->setLink(l);

	if (btime){
		cout << "Link Vector	->	OK";
		tend=time(NULL);
		tlink=difftime(tend,tmid);
		cout <<  "	Done in : " << tlink << "s" << endl;
		tmid = tend;
	}

	if (result){
		cout << endl << "link vector : " ;
		this->wt->printLink();
	}

	makeWt(tags);


	if (btime){
		cout << endl << "WT 		-> 	OK";
		tend=time(NULL);
		twt=difftime(tend,tmid);
		cout << "	Done in : " << twt << "s" << endl << endl;
		tmid = tend;
	}
	if (result)
		visuWt();

	delete arb;
	tags.clear();
}


TLIndex::~TLIndex(){
// 	mr_destroy(mr);
// 	this->c.clear();
}


/********************
	Initialisation
********************/


int TLIndex::changeLettreBwtRope(int pos, uint16_t code, vector<uint16_t> &tags){
	tags[pos] = code;
	int fol = this->b->followingLetter(pos);
	return fol;
}


void TLIndex::lettreToTagRope(ArbreVDJ* arb, string fichierIn, vector<uint16_t> &tags, bit_vector &seq){
	ifstream fichier(fichierIn, ios::in);
    if(fichier) { 
    	vector<Annotation*> vecTags; //labels of a sequence
    	int ivec; //position in the annotation vector
    	string ligne;
    	int pos; // position of the letter we want to change in the bwt
    	int j; //position on the read
		int taille; //length of the read
		int total = 0; //taille size of the seq currently read
		uint16_t currentCode; // code of the current label
		string currentLabel;
		int ajoutLabel; //number of labels in the current sequences

		for (int i=0; i<this->b->getC(1); i++){ // read the sequences
			getline(fichier, ligne);
			while (ligne[0] != '>')	
				getline(fichier, ligne);

			seq[total]=1;
			ajoutLabel = vectorTag(vecTags, arb, ligne);
			getline(fichier, ligne);
			if (ajoutLabel>0){
				taille = ligne.length();
				ivec = vecTags.size()-1;

				pos = i;
				j=taille-1;		
				currentCode = 65535; //TODO : TAGNULL
				int cpt=j- vecTags[ivec]->getFin();
				for (int m=0; m<cpt; m++){	//portion between the end of the read and the last label
					pos = changeLettreBwtRope(pos, currentCode, tags);
					j--;
				}
			
				while((ivec>=(((int)vecTags.size() - ajoutLabel) - 1)) &&(j>=0)){
					currentLabel = vecTags[ivec]->getName();
					this->wt->ajoutTagInMap(arb, currentLabel);
					if(this->wt->countMapLabel(currentLabel)>0)
						currentCode = this->wt->getCodeFromName(currentLabel);
					else{
						currentCode = arb->findCell(currentLabel)->getCode();
						this->wt->ajoutCodeinMap(currentLabel, currentCode);
					}
					while(j>= vecTags[ivec]->getDebut()){ // length of the label
						pos = changeLettreBwtRope(pos, currentCode, tags);
						j--;
					}
					ivec--;
					currentCode = 65535; //TODO : TAGNULL

					if (ivec>=0){ // between 2 tags
						cpt= vecTags[ivec]->getFin();
					}
					else		// before the first tag
						cpt = -1;
					while(j>cpt){
						pos = changeLettreBwtRope(pos, currentCode, tags);
						j--;
					}
				}
				//add the NULL symbol at the end $
				// currentCode = 65535; //TODO : TAGNULL
				// tags[pos] = currentCode;
				// vecTags.clear();
			}
			total+=taille+1;
		}
		// ~vecTags;

	} else 
		cout << "Error when trying to open the file " << fichierIn << endl; 	
}


void TLIndex::makeLink(vector<uint16_t> &tags, bit_vector &b){ 
	for (int i=1; i<tags.size(); i++){
		if (tags[i] == tags[i-1])
			b[i] = 0;
		else
			b[i] = 1;
	}
}

vector<uint16_t> TLIndex::makeRacineWt(vector<uint16_t> &tags){
	vector<uint16_t> vecRacine;
	vecRacine.push_back(tags[0]);
	int i=1;
	while(i<tags.size()){
		if (tags[i] != tags[i-1])
			vecRacine.push_back(tags[i]);
		i++;
	}
	return vecRacine;
}


void TLIndex::makeWt(vector<uint16_t> &tags){
	vector<uint16_t> racine = makeRacineWt(tags);
	this->wt->makeWt(racine);
}



void TLIndex::initWt(WTAnno *w){
	this->wt = w;
}

void TLIndex::setSeq(bit_vector seq){
	rrr_vector<127> rrrs(seq);
	this->seq = rrrs;
	rrr_vector<127>::rank_1_type r(&this->seq);
	this->s_rank = r;
	rrr_vector<127>::select_1_type s(&this->seq);
	this->s_sel = s;
}

/***************************
		Queries

Abbreviations of the complexities : 
	-rank2a : complexity of rank2a : lengthRope + sizeLeafRope
	-select : same
	-w : heigth of the WT (=log(nb of leaves))

***************************/


//pos bwt 	-> pos root wt : O(1) 	\
//pos root 	-> leaf wt : O(w)	|>	O(w)	
//leaf		-> label : O(1)			/
string TLIndex::labelU(int pos){
	return this->wt->getLabel(pos);
}

//pos text 	-> pos bwt : O(log(n)^(1+ \epsilon))
//pos bwt 	-> pos root wt : O(1) 	\
//pos root 	-> leaf wt : O(w)	|>	O(w)	
//leaf		-> label : O(1)			/
string TLIndex::label(int pos){
	pair<int, int> p = findNumSeq(pos); //translation pos tex -> pos bwt
	int i= beginSeq((p.first)+1) - beginSeq(p.first) - p.second-2;
	int pp;
	if (i>=0)
		pp = this->b->findPosFromNumSeq(p.first, i);
	else
		pp = this->b->previousLetter(p.first);
		return this->wt->getLabel(pp);
}


// theorique : O(motif)
//real : pattern -> range of occurrences of the pattern : O(pattern*lengthRope)
vector<int> TLIndex::findM(string motif){
	int debut=0;
	int fin= this->taille;
	this->b->findMotif(motif, &debut, &fin);
	vector<int> res;
	int pos;
	for (int i=debut; i<fin; i++){
		pos = b->previousLetter(i);
		res.push_back(pos);
	}
	return res;
}

vector<int> TLIndex::findMotif(string motif){
	vector<int> res = findM(motif);
	return posBwtToT(res);
}


// annotation -> liste n° seq which have anno : O(occ1*w + occ2)
// occ1 : number of occurences found at the root of the WT
// occ2 : number of occurences in the BWT (occ2 >= occ1)
vector<int> TLIndex::findL(string anno){
	vector<int> pos = this->wt->getPositions(anno);
	return posBwtToT(pos);
}


//pattern + anno ->> list if the positions which have the pattern and the label : 
// we find the positions which begins the pattern, for all of them we check the label
// real : O(pattern*lengthRope + occ1*w)
// theorique : O(pattern+ occ1*w)
vector<int> TLIndex::findMotifXAnno(string motif, string anno){
	vector<int> vmotif = findM(motif);
	vector<int> pos = this->wt->findMotifXAnno(anno, vmotif);

	return posBwtToT(pos);
}


/********************************
	Accessors to the sequences
********************************/


//pos bwt -> n° sequence : O(select)
int TLIndex::numSeq(int pos){
	return this->b->findNumeroSeqFromPos(pos).first;
}


//n° seq -> list of labels of seq (read * hauteurWt)
vector<Annotation*> TLIndex::findAnnoFromNumber(int numero){
	vector<Annotation*> vecAnno;
	int pos = numero;
	string anno;
	string annoprec = labelU(pos);
	int debut = 0, fin = 0;

	do{
		anno = labelU(pos);
		
		if (anno != annoprec){
			if (annoprec != "NOTAG"){
				Annotation *a = new Annotation (annoprec, debut, fin-1);
				vecAnno.push_back(a);
			}
			debut = fin;
		}
		fin++;
		pos = this->b->followingLetter(pos);
		annoprec = anno;
	} while(pos >= this->b->getC(1));

	// seq has been read in the reverse direction, we have to put it back in the right way
	vector<Annotation*> vec;
	for (int i=vecAnno.size()-1; i>=0; i--){
		Annotation *a = new Annotation(vecAnno[i]->getName(), fin-1-vecAnno[i]->getFin()-1, fin-1-vecAnno[i]->getDebut()-1);
		vec.push_back(a);
	}
	return vec;
}


vector<int> TLIndex::posBwtToWt(vector<int> motif){
	vector<int> res;
	for (int i=0; i<motif.size(); i++)
		res.push_back(this->wt->posBwtToWt(motif[i]));
	return res;
}

vector<int> TLIndex::posBwtToT(vector<int> pos){
	vector<int> res;
		pair<int, int> p;
		int pp;
	for (int i=0; i<pos.size(); i++){
		p = this->b->findNumeroSeqFromPos(pos[i]);
		pp = beginSeq(p.first+1) - p.second;
		res.push_back(pp);
	}
	return res;
}

int TLIndex::beginSeq(int numSeq){
	assert(numSeq <= this->s_rank(this->taille));
	int pos = this->s_sel(numSeq+1);
	if (pos > this->seq.size())
		pos = this->seq.size();
	return pos;
}

pair<int, int>TLIndex::findNumSeq(int pos){
	assert(pos < this->taille);
	int seq = this->s_rank(pos);
	if (seq == this->s_rank(pos+1))
		seq--;
	int posSeq = pos - beginSeq(seq);
	pair<int, int> p (seq, posSeq);
	return p;
}


/******************************
	Store/Restore
******************************/

void TLIndex::saveAll(string file){
	this->b->saveBwt(file);
	this->wt->saveAnno(file);
}


void TLIndex::restoreIndex(string file){	
	BWT *b1 = new BWT();
	b->restoreBwt(file);
	this->b = b1;

	this->taille = this->b->getTaille();

	WTAnno *wt = new WTAnno(file);
	// wt->restoreWt(file);
	this->wt = wt;
}


/***************************
		Visualisation
***************************/

void TLIndex::visuWt(){
	this->wt->visuWt();
}


void TLIndex::seqOrigine(){
	cout << "Number of letters : " << this->taille << endl;
	int pos = 0;
	for (int i=0; i<this->taille; i++){
		cout << pos << " " ;
		pos = this->b->followingLetter(pos);
	}
	cout << endl;
}


void TLIndex::printLabels() const{
	this->wt->printLabels();
}


/***********************
	Getter/Setter
***********************/

int TLIndex::nbLettresRoot() const {
	return this->wt->nbLettresRoot();
}

int TLIndex::nbAnnoDiff() const{
	return this->wt->sigma();
}

string TLIndex::getLabel(int i) const{
	pair<string, int> p =  this->wt->getElementMap(i);
	return p.first;
}

int TLIndex::sizeAnno() const{
	return this->wt->sizeAnno();
}

int TLIndex::sizeLink() const{
	return this->wt->sizeLink();
}

int TLIndex::sizeHelper() const{
	return this->wt->sizeMap();
}

string TLIndex::sequence(int i){
	return b->seq(i);
}

