#ifndef MAPINDEX_H
#define MAPINDEX_H


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <sdsl/wavelet_trees.hpp>

#include "annotation.hpp"
#include "mapAnno.hpp"

#include "index.hpp"


using namespace std;
using namespace sdsl;

class MapIndex : public Index{

	private:
		MapAnno *ht;
		rrr_vector<127> seq; // start of the sequences
		rrr_vector<127>::rank_1_type s_rank;
		rrr_vector<127>::select_1_type s_sel;
		
	public:
	//restore from file
	MapIndex(string file, bool result);
	MapIndex(string dossier, string file, bool btime, bool result);
	~MapIndex();
	void setSeq(bit_vector l);

	//queries
	// returns the letter at position pos int the BWT
	string labelU(int pos);
	string label(int pos);
	// gives the begining and end of the area wich have the pattern "motif"
	vector<int> findMotif(string motif);
	// returns all the text positions which correspond to the label
	vector<int> findL(string anno);
	// returns the ids of the sequences which have a pattern labelles anno
	vector<int> findMotifXAnno(string motif, string anno);

	//accessors
	// from a position in the BWT, returns the sequence's id
	int numSeq(int pos);
	// from positions of the BWT, gives the ids of the sequences
	vector<int> numSeq(vector<int> pos);
	int beginSeq(int numSeq);
	int getPosition(pair<int, int> p);
	vector<int> getPositions(vector<pair<int, int>> p);
	vector<Annotation*> findAnnoFromNumber(int numero);
	//from a position of the BWT, returns the corresponding position in the WT's root
	int posBwtToWt(int pos);	
	vector<int> posBwtToWt(vector<int> motif);

	//store / restore
	void saveAll(string file);

	//visualisation
	void visuWt();

	int nbLettresRoot() const;
	int nbAnnoDiff() const;
	string getLabel(int i) const;
	int sizeAnno() const;
	int sizeLink() const;
	int sizeHelper() const;
	void printLabels() const;


};
#endif