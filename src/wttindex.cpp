#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <tuple>
#include <cassert>
#include <map>
#include <vector>
#include <bitset>
#include <cstdint>
#include <ctime> 

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

#include "wttindex.hpp"

#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include "tlIndex.hpp"
#include "outils.hpp"
#include "wtAnno.hpp"
#include "visu.cpp"
#include "tlbwt.hpp"

// uint16_t TAGNULL = 65535; // max : 1111111111111111

using namespace std;
using namespace sdsl;


/****************
  Construction
****************/


  WTTIndex::WTTIndex(string dossier, string file, bool btime, bool result) : Index(dossier, file, btime, result){
	ArbreVDJ *arb = new ArbreVDJ();
	arb->constructionArbre(dossier);
	if (btime)
		cout << "Germlines Tree	->      OK" << endl;

	time_t tend, tmid;
    double texec=0.;
    double tlink=0., twt=0.;
    tmid=time(NULL);
	WTAnno *wt = new WTAnno();
	this->wt = wt;
	vector<uint16_t> tags;

	bit_vector l = bit_vector(taille, 0);
	bit_vector seq = bit_vector(taille, 0);
	makeRacineLink(l, seq, tags, file, arb);
	this->wt->setLink(l);
	setSeq(seq);

	if (btime){
		cout << "Link Vector	->	OK";
		tend=time(NULL);
		tlink=difftime(tend,tmid);
		cout <<  "	Done in : " << tlink << "s";
		tmid = tend;
	}
	if (result){
		cout << "link vector :	" ;
		wt->printLink();
		cout << "seq vector :	" ;
		printSeq();
		cout << "Labels : "; 
		for (int i = 0; i<tags.size(); i++)
			cout << tags[i] << " " ;
		cout << endl;
	}

	this->wt->makeWt(tags);

	if (btime){
		cout << endl << "WT 		-> 	OK";
		tend=time(NULL);
		twt=difftime(tend,tmid);
		cout << "	Done in : " << twt << "s" << endl;
		tmid = tend;
	}

	if (result)
		visuWt();

	delete arb;
	tags.clear();
}


// the label is from begin to end (included) 
// so we put 0-bits between begin+1 and end (included)
void WTTIndex::majLink(bit_vector &l, int begin, int end){
	for (int i=begin+1; i<=end; i++)
		l[i]=0;
}


void WTTIndex::makeRacineLink(bit_vector &l, bit_vector &seq, vector<uint16_t> &tags, string file, ArbreVDJ* arb){
ifstream fluxLec(file.c_str());  
    if(fluxLec){
    	string line;
    	vector<Annotation*> vecTags; //labels of a sequence    	
    	int ivec=0; //position in vecTags
    	int ajoutLabel; //number of labels in the current sequences
    	int posL = 0; //position in link
    	int beginAnno=0;
    	int endAnno=-1;
    	int total=0;
    	 
    	while(getline(fluxLec, line)){
			ajoutLabel = vectorTag(vecTags, arb, line);
			seq[total]=1;    
			while(ivec<vecTags.size()){
				beginAnno = vecTags[ivec]->getDebut() + total;
				if (endAnno+1 < beginAnno){	// there is some not labeled letters between the end of the last sequence and the begin for the current one
					tags.push_back(this->wt->getCodeFromName("NOTAG"));
					l[endAnno+1] = 1;
					// majLink(l, endAnno+1, beginAnno-1);
				}
				endAnno = vecTags[ivec]->getFin() + total;
				this->wt->ajoutTagInMap(arb, vecTags[ivec]->getName());
				tags.push_back(this->wt->getCodeFromName(vecTags[ivec]->getName()));
				l[beginAnno] = 1;
				// majLink(l, beginAnno, endAnno);
				ivec++;
			}
			getline(fluxLec, line); //DNA sequence	
			total += line.size()+1;
		}

		if (endAnno+1 < total){	// there is some not labeled letters at the end of the final sequence
			tags.push_back(this->wt->getCodeFromName("NOTAG"));
			l[endAnno+1] = 1;
			// majLink(l, endAnno+1, beginAnno-1);
		}

    	fluxLec.close();
	} else
        cerr << "Save Map : Error when trying to open file (reading) : " << file << endl;
}


void WTTIndex::setSeq(bit_vector seq){
	rrr_vector<127> rrrs(seq);
	this->seq = rrrs;
	rrr_vector<127>::rank_1_type r(&this->seq);
	this->s_rank = r;
	rrr_vector<127>::select_1_type s(&this->seq);
	this->s_sel = s;
}

/**************
	Queries
**************/


//pos bwt 	-> pos text : O(log(n)^(1+epsilon)) 
//pos text 	->	pos root wt : O(1) 	\
//pos root 	-> leaf wt : O(w)		|>	O(log(n)^(1+epsilon) + w)	
//leaf		-> label : O(1)			/
string WTTIndex::labelU(int pos){
	pair<int, int> p = this->b->findNumeroSeqFromPos(pos);
	int pp;
	int tailleSeq = beginSeq(p.first+1) - beginSeq(p.first);
	if (p.second >= tailleSeq-1)
		pp = beginSeq(p.first+1)-1;
	else
		pp  = beginSeq(p.first+1) - p.second-2;
	// cout << endl << pos << " -> " << p.first << " " << p.second << " -> " << pp << " " ;
	return this->wt->getLabel(pp);
}

//pos text 	-> pos root wt : O(1) 	\
//pos root 	-> leaf wt : O(w)		|>	O(w)	
//leaf		-> label : O(1)			/
string WTTIndex::label(int pos){
	return this->wt->getLabel(pos);
}


// annotation -> liste n° seq which have anno : O(occ1*w + occ2)
// occ1 : number of occurences found at the root of the WT
// occ2 : number of occurences in the BWT (occ2 >= occ1)
vector<int> WTTIndex::findL(string anno){
	return this->wt->getPositions(anno);
}


// theorique : O(motif)
//real : pattern -> range of occurrences of the pattern : O(pattern*lengthRope)
vector<int> WTTIndex::findMotif(string motif){
	int debut=0;
	int fin= this->taille;
	this->b->findMotif(motif, &debut, &fin);
	vector<int> res;
	int pos;
	for (int i=debut; i<fin; i++){
		pos = b->previousLetter(i);
		res.push_back(posBwtToWt(pos));
	}
	return res;
}

//pattern + anno ->> list if the positions which have the pattern and the label : 
// we find the positions which begins the pattern, for all of them we check the label
// real : O(pattern*lengthRope + occ1*w)
// theorique : O(pattern+ occ1*w)
vector<int> WTTIndex::findMotifXAnno(string motif, string anno){
	vector<int> vmotif = findMotif(motif);
	vector<int> lettres;

	for (int i=0; i<vmotif.size(); i++){
		if (label(vmotif[i]) == anno)
			lettres.push_back(vmotif[i]);
	}
	return lettres;
}

int WTTIndex::posBwtToWt(int pos){
	pair<int, int> p = this->b->findNumeroSeqFromPos(pos);
	return beginSeq(p.first+1) - p.second;
}


vector<int> WTTIndex::posBwtToWt(vector<int> motif){
	vector<int> v;
	pair<int, int> p;
	int pos;
	for (int i=0; i<motif.size(); i++){
		p = this->b->findNumeroSeqFromPos(motif[i]);
		pos = beginSeq(p.first+1) - p.second;
		v.push_back(pos);
	}
	return v;
}


/********************************
	Accessors to the sequences
********************************/



//pos bwt -> n° sequence : O(select)
int WTTIndex::numSeq(int pos){
	assert(pos < this->taille);
	int p = this->s_rank(pos);
	if (p == this->s_rank(pos+1))
		p--;
	return p;
}


int WTTIndex::beginSeq(int numSeq){
	assert(numSeq <= this->s_rank(this->taille));
	int pos = this->s_sel(numSeq+1);
	if (pos > this->seq.size())
		pos = this->seq.size();
	return pos;
}



vector<Annotation*> WTTIndex::findAnnoFromNumber(int numero){
	vector<Annotation*> vecAnno;
	int startSeq = beginSeq(numero);
	int endSeq = beginSeq(numero+1);
	string anno = label(startSeq);
	int begin=startSeq;

	for (int i=startSeq+1; i<endSeq; i++){
		if (label(i) != anno){
			if (anno != "NOTAG"){
				Annotation *a = new Annotation(anno, begin, i-1);
				vecAnno.push_back(a);
			}
			begin = i;
			anno = label(i);
		}
	}
	return vecAnno;
}



/******************************
	Store/Restore
******************************/

void WTTIndex::saveAll(string file){
	this->b->saveBwt(file);
	this->wt->saveAnno(file);
}



/***************************
	Visualisation / Sizes
***************************/

void WTTIndex::visuWt(){
	this->wt->visuWt();
}

void WTTIndex::printLabels() const{
	this->wt->printLabels();
}


int WTTIndex::nbLettresRoot() const {
	return this->wt->nbLettresRoot();
}

int WTTIndex::nbAnnoDiff() const{
	return this->wt->sigma();
}


string WTTIndex::getLabel(int i) const{
	pair<string, int> p =  this->wt->getElementMap(i);
	return p.first;
}

int WTTIndex::sizeAnno() const{
	return this->wt->sizeAnno();
}

int WTTIndex::sizeLink() const{
	return this->wt->sizeLink();
}

int WTTIndex::sizeHelper() const{
	return this->wt->sizeMap();
}

void WTTIndex::printSeq() const{
	cout << this->seq<< endl;
}
