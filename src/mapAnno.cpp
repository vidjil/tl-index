#include <iostream>
#include <fstream>
#include <string>

#include "mapAnno.hpp"

using namespace std;
using namespace sdsl;

MapAnno::MapAnno(string fileIn, int taille){
	map<string, vector<tuple<int, int, int> > > h;	
	this->ht = h;
	vector<tuple<int, int, int> > v;
	ht["NOTAG"] = v;
}


MapAnno::~MapAnno(){
	// delete this->ht
}

void MapAnno::addAnno(string name, int seq, int begin, int end){
	tuple<int, int, int> p (seq, begin, end);
	map<string, vector<tuple<int, int, int> > >::iterator it = ht.find(name);
	if(it == ht.end()){
 		vector<tuple<int, int, int> > v;
 		v.push_back(p);
		ht[name] = v;
	}
	else
		ht[name].push_back(p);
}

void MapAnno::addInM(string name){
	this->m.push_back(name);
}

void MapAnno::setLink(bit_vector l){
	rrr_vector<127> rrrl(l);
	this->link = rrrl;
	rrr_vector<127>::rank_1_type r(&this->link);
	this->b_rank = r;
}


/**************
	Queries
**************/

//returns the label of the position pos
string MapAnno::getLabel(int pos){
	int p;		// p = nombre de 1 avant pos dans lien
	p =	this->b_rank(pos);
	if (p == this->b_rank(pos+1))
		p--;
	return m[p];
}

//returns the positions of the label l
vector<pair<int, int> > MapAnno::getPositionsMap(string name){
	vector<pair<int, int> > res;
	map<string, vector<tuple<int, int, int> > >::iterator it = ht.find(name);
	if(it != ht.end()){
		vector<tuple<int, int, int>> v = ht[name];
		tuple<int, int, int> t;
		for (int j=0; j<v.size(); j++){
			t = v[j];
			for (int i=get<1>(t); i<get<2>(t)+1; i++){
				pair<int, int> p(get<0>(t), i);
				res.push_back(p);
			}
		}
	}
	return res;
}


/********************************
	Accessors to the sequences
********************************/




/******************************
	Store/Restore
******************************/

//save the HT
void saveHt(string fileHt){
	;
}

// //retore the HT
map<string, pair<int, int>> restoreHt(string fileHt){
	;
}


/***************************
	Visualisation / Sizes
***************************/

void MapAnno::visuHt(){
	cout << "Link : " << link << endl;
	cout << "M : ";
	for (int i=0; i<m.size(); i++)
		cout << m[i] << " ";
	cout << endl;
	cout << "Labels : " << endl;
	printHt();
}

int MapAnno::sigma() const{
	return this->ht.size();
}

int MapAnno::sizeHt() const{
	int size=0;
	for (map<string, vector<tuple<int, int, int> > >::const_iterator it=this->ht.begin(); it!=this->ht.end(); ++it){
    	size += it->first.size() + it->second.size() * 3*sizeof(int);
	}		
    return size;
}


int MapAnno::sizeLink()const{
	return size_in_bytes(this->link) + size_in_bytes(this->b_rank) + sizeM();
}

int MapAnno::sizeM()const{
	// int size=0;
	// for (int i=0; i<m.size(); i++)
	// 	size+=m[i].size();
	// return size;
	return (m.size()* sizeof(uint16_t)); 
}


void MapAnno::printHt() const{		
	for (map<string, vector<tuple<int, int, int> > >::const_iterator it=this->ht.begin(); it!=this->ht.end(); ++it){
    	cout << it->first << " : " << it->second.size() << " : " ;
    	for (int i=0; i<it->second.size(); i++)
    		// cout << i << " " ;
    		cout << "(" << get<0>(it->second[i]) << ", " << get<1>(it->second[i]) << ", " << get<2>(it->second[i]) << ") ";
	cout << endl;
	}
}

string MapAnno::getLabel(int i) const{		
	map<string, vector<tuple<int, int, int> > >::const_iterator it=this->ht.begin();
	int j=0;
	while(it != this->ht.end() && j<this->ht.size()){
	if (i==j)
		return it->first;
	j++;
	it++; 
	}
}