#ifndef ARBREVDJ_H
#define ARBREVDJ_H

#include <string>
#include <iostream>
#include <vector>
#include "cell.hpp"

//int GERMLINE[6] = {28, 24, 16, 8, 4, 0};

using namespace std;


class ArbreVDJ{

private: 
	Cell* root;

public:
	ArbreVDJ();
	~ArbreVDJ();


	// depuis la ligne de commentaire fasta du gène, récupère le nom du gène, placé entre '|'
	string nomGene (string ligne);
	// divise le nom du gène en étage de l'arbre : hiérarchisation
	vector<string> diviserNom(string name);
	// ajoute le gène à l'arbre des VDJ
	void ajouterGene(string name);
	// met dans l'arbre tous les gènes présents dans un fichier fasta
	void chargerGermline (string file);
	/* Les germlines sont rentrées dans un certain ordre pour que les codes 
	(en 16 bits) des feuilles de l'arbre se fassent correctement : 
	TR =0, IG = 1,
	A et H = 0, B et K = 1, D et L = 2, G = 3,
	V = 0, J = 1, D et KDE = 2, Intron = 3
	Les autres bits sont mis dans l'ordre d'apparition du gène dans le germline
	*/
	void constructionArbre(string dossier);


	// retourne la cellule de bon nom, sinon la racine
	Cell* findCell(string name);
	// retourne la cellule ancêtre la plus proche de la cellule souhaitée si celle-ci n'existe pas 
	Cell* findFourche(string name);


	int nbFeuilles();
	void nbMaxFilsParEtage();
	

	void setFreq(Cell* c, int freq);
	
	// print les neuds de l'arbre par étage
	void toString();



	
};

#endif
