#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <typeinfo>
#include <queue>
#include "cell.hpp"
#include "arbreVDJ.hpp"



using namespace std;



ArbreVDJ::ArbreVDJ(){
	Cell *root;
	root = new Cell("root", 0);
	this->root = root;
}

ArbreVDJ::~ArbreVDJ(){
	for (int i=this->root->getFils().size()-1; i>=0; i--){
		delete this->root->getFils(i);
	}
}

/************************
	Init de l'arbre
************************/

string ArbreVDJ::nomGene (string ligne){
	string name;
	int i=0;
	while (ligne[i]!='|')
		i++;	
	i++;
	while (ligne[i] != '|'){
		name +=ligne[i];
		i++;
	}

	return name;
}

//diviser le nom en étage de l'arbre
vector<string> ArbreVDJ::diviserNom(string name){

	vector<string> res = vector<string>();
	if ((name == "KDE") || (name == "Intron")){
		res.push_back("IGK");
		res.push_back(name);
		return res;
	}

	assert (name.size() > 4);
	string str1 (name.begin(), name.begin()+3);
	res.push_back(str1);  	// TRG, IGH, ...
	string str2 = "";
	str2 += name[3];			//V, D, J

	res.push_back(str2);

	string str3 = "";
	for (int i=4; i<name.size(); i++)
		str3 += name[i];
	res.push_back(str3);
	return res;

}

void ArbreVDJ::ajouterGene(string name){

	Cell *fourche = findFourche(name);
	vector<string> div = diviserNom(name);
	string id;
	for (int i=fourche->getProfondeur(); i<div.size(); i++){
			id = div[i];
			Cell *fils = new Cell(id, (i+1));
			uint16_t codeFils = fourche->calculerCode();
			fils->setCode(codeFils);
			fourche->addFils(fils);
			fourche = fils;
		
	}
}


void ArbreVDJ::chargerGermline (string file){

	ifstream fichier(file, ios::in);

	if(fichier) {
		string ligne;
		while (getline(fichier, ligne)){
		//for (int i=0; i<3; i++){
		//	getline(fichier, ligne);
			if (ligne[0]== '>'){
				
				string nom = nomGene(ligne);
				ajouterGene(nom);
			}
		}
		
		fichier.close();
	
	} else
	cerr << "Couldn't open file " << file << endl;	

}


void ArbreVDJ::constructionArbre(string dossier){

	//chargerGermline("./Germlines/homosapiens/GermlineTest.fa");
	dossier.erase(dossier.end()-4, dossier.end());
	chargerGermline(dossier + "../germline/homo-sapiens/TRAV.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRAJ.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRBV.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRBJ.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRBD.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRBD_upstream.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRDV.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRDJ.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRDD.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRDD2_upstream.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRDD3_downstream.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRDD_upstream.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/TRGV.fa"); 
	chargerGermline(dossier + "../germline/homo-sapiens/TRGJ.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGHV.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGHJ.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGHD.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGHD_upstream.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGKV.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGKJ.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGK-KDE.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGK-INTRON.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGLV.fa");
	chargerGermline(dossier + "../germline/homo-sapiens/IGLJ.fa");/**/
}



/***************************
	Recherche dans l'arbre
***************************/


Cell* ArbreVDJ::findFourche(string c){
	vector<string> div = diviserNom(c);
	Cell *current = this->root;
	bool trouve; 
	int j;
	string id;
	for (int i=0; (unsigned) i<div.size(); i++){
		trouve = false;
		j=0;
		while(!trouve && (unsigned) j<current->getFils().size()){
			if (current->getFils(j)->getId() == div[i]){
				trouve = true;
				current= current->getFils(j);
			}
			j++;
		}
		if (!trouve)
			return current;
	}
	return current;
}

Cell* ArbreVDJ::findCell(string name){
	Cell *res = findFourche(name);
	if (res->estFeuille())
		return res;
	return this->root;
}


/***************************
	Stats de l'arbre
***************************/


int ArbreVDJ::nbFeuilles(){
	int cpt=0;
	queue<Cell*> file;
	file.push(this->root);
	Cell *current;

	while(file.size() > 0){
		current = file.front();
		file.pop();
		cpt++;
		for (int i=0; (unsigned) i<current->getFils().size(); i++)
			file.push(current->getFils(i));
	}
	return cpt;
}


void ArbreVDJ::nbMaxFilsParEtage(){
	queue<Cell*> file;
	file.push(this->root);
	Cell *current;
	int etage[6];
	for (int j=0; j<6; j++)
		etage[j]=0;
	int i=0;

	while(file.size() > 0){
		current = file.front();
		file.pop();
		
		if (current->getProfondeur() > i){
			i++;
		}

		if ((unsigned) etage[i] < current->getFils().size())
			etage[i] = current->getFils().size();

		for (int j=0; (unsigned) j<current->getFils().size(); j++)
			file.push(current->getFils(j));
	}
	cerr << "okay" << endl;
	cerr << "max des etages" << endl;
	for (int j=0; j<6; j++)
		cerr << etage[j] << " ";
	cerr << endl;
}



/*************************
		Accesseurs
*************************/


void ArbreVDJ::setFreq(Cell* c, int freq){

	c->setFreq(freq);
	cout << "freq de " << c->getId() << " : " << c->getFreq() << endl;
}



/***********************
		Affichage
***********************/


void ArbreVDJ::toString(){

	cerr << "ArbreVDJ : \n" << endl;
	queue<Cell*> file;
	file.push(this->root);
	Cell *current;
	int prof=0;
	int cpt=0; 

	while(file.size()>0){
		current = file.front();
		file.pop();
		if (current->getProfondeur() > prof){
			cerr << endl << "Etage " << prof << " : " << cpt << " noeuds" << endl << endl;
			prof++;
			cpt=0;
		}
		cerr << current->toString() << " ";
		for (int i=0; (unsigned) i<current->getFils().size(); i++){
			file.push(current->getFils(i));
		}
		cpt++;
	}
	cerr << endl << "Etage " << prof << " : " << cpt << " noeuds" << endl;
}



