#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <bitset>
#include <cmath>
#include "cell.hpp"


uint16_t CODENULL = 65535; // max : 1111111111111111
int SEPARATION[4] = {16,13, 11, 0};

using namespace std;


	//Cell::Cell (string id, int prof, uint32_t u){
	Cell::Cell(string id, int prof){
		this->identifiant = id;
		this->profondeur = prof;
		this->fils = vector<Cell*>();
		this->code = CODENULL;
		this->freq = 0;
	}

	Cell::~Cell(){
		for (int i=0; i<this->fils.size(); i++)
			delete this->fils[i];
		//this->fils.clear();
	}

	uint16_t Cell::calculerCode(){
		int nbFrere = this->fils.size();
		int profondeur = this->getProfondeur();
		int zero = (pow(2, SEPARATION[profondeur]) - pow(2, SEPARATION[profondeur+1]));
		bitset<16> y(zero);
		int tmp = nbFrere*pow(2, SEPARATION[profondeur+1]);
		bitset<16> z(tmp);
		uint16_t code = this->code - zero + tmp;
		bitset<16> x(code);
		return code;
	}

	void Cell::addFils (Cell* c){
		this->fils.push_back(c);
	}

	bool Cell::estFeuille(){
		if (this->fils.size() == 0)
			return true;
		return false;
	}



	string Cell::getId() const{
		return this->identifiant;
	}

	int Cell::getProfondeur() const{
		return this->profondeur;
	}

	Cell* Cell::getFils (int indice) const{
		return this->fils[indice];
	}

	vector<Cell*> Cell::getFils() const{
		return this->fils;
	}

	uint16_t Cell::getCode() const{
		return this->code;
	}

	int Cell::getFreq() const{
		return this->freq;
	}



	void Cell::setId(string id) {
		this->identifiant = id;
	}

	void Cell::setProfondeur(int prof) {
		this->profondeur = prof;
	}

	void Cell::setFils(vector<Cell*> vec) {
		this->fils = vec;
	}

	void Cell::setCode(uint16_t c){
		this->code = c;
	}

	void Cell::setFreq(int f){
		this->freq = f;
	}



	string Cell::toString() const{
		std::stringstream sstm;
		bitset<16> x(code);
		sstm << "(" << identifiant << ", " << fils.size() << ", " << x << ")";
		return sstm.str();
	}

	string Cell::tableFils() const {

	std::stringstream sstm;
	sstm << "[";
	for (int i=0; (unsigned) i<this->fils.size(); i++){
		//Cell *f = this->fils[i];
		//sstm << f->getId();
		//sstm << *f->toString();
	}
	sstm << "]";
	return sstm.str();

}