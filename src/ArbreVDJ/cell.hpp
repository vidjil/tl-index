#ifndef CELL_H
#define CELL_H

#include <string>
#include <iostream>
#include <vector>
#include <cstdint>

using namespace std;


class Cell {
	private:
	string identifiant; 
	int profondeur; 
	vector <Cell*> fils;
	uint16_t code;
	int freq;

	public:
	Cell(string id, int prof);
	~Cell();

	//bool estBrancheFeuille();
	//bool estFourche();

	// /!\ Bien faire le calcul avant le lien avec la cell parent
	uint16_t calculerCode();

	void addFils (Cell* c);
	bool estFeuille();
	//void freqPlus1();

	string getId() const;
	int getProfondeur() const;
	Cell* getFils (int indice) const;
	vector<Cell*> getFils() const;
	uint16_t getCode() const;
	int getFreq() const;

	void setId(string id);
	void setProfondeur(int prof);
	void setFils(vector<Cell*> vec);
	void setCode(uint16_t c);
	void setFreq(int f);

	string toString() const;
	string tableFils() const ;


};

#endif