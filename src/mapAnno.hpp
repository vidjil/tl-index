#ifndef HTANNO_H
#define HTANNO_H


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>


using namespace std;
using namespace sdsl;

class MapAnno{

private:
	map<string, vector<tuple<int, int, int> > > ht ; //sequence, begin, end of the label
	vector<string> m;	//rle of the labels
	rrr_vector<127> link;	// decode the rle m
	rrr_vector<127>::rank_1_type b_rank;

public:
	MapAnno(string fileIn, int taille);
	~MapAnno();
	void addAnno(string name, int seq, int begin, int end);
	void addInM(string name);
	void setLink(bit_vector l);

	//returns the label of the position pos
	string getLabel(int pos);

	//returns the positions of the label l
	vector<pair<int, int> > getPositionsMap(string name);



	//save the HT
	void saveHt(string fileHt);

	// //retore the HT
	map<string, pair<int, int>> restoreHt(string fileHt);

	void visuHt();
	int sigma() const;
	int sizeHt() const;
	int sizeLink()const;
	int sizeM()const;
	void printHt() const;
	string getLabel(int i) const;


};


#endif