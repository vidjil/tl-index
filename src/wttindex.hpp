#ifndef WTTINDEX_H
#define WTTINDEX_H


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <sdsl/wavelet_trees.hpp>

#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include "annotation.hpp"
#include "wtAnno.hpp"
#include "tlbwt.hpp"
#include "tlbwt.hpp"

#include "index.hpp"

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

using namespace std;
using namespace sdsl;

class WTTIndex : public Index{

	private:
		WTAnno *wt;
		rrr_vector<127> seq;
		rrr_vector<127>::rank_1_type s_rank;
		rrr_vector<127>::select_1_type s_sel;

public:
	WTTIndex(string file, bool result);
	WTTIndex(string dossier, string file, bool btime, bool result);
	//put 0-bit from begin+1 to end (included)
	void majLink(bit_vector &l, int begin, int end);
	//make the link and the root of the WT in the order of the text
	void makeRacineLink(bit_vector &l, bit_vector &seq, vector<uint16_t> &tags, string file, ArbreVDJ* arb);
	void setSeq(bit_vector l);

	//queries
	string labelU(int pos);
	string label(int pos);
	vector<int> findL(string anno);
	vector<int> findMotif(string motif);
	int posBwtToWt(int pos);
	vector<int> posBwtToWt(vector<int> motif);
	// returns the positions which have a pattern labelled anno
	vector<int> findMotifXAnno(string motif, string anno);

	int numSeq(int pos);
	vector<Annotation*> findAnnoFromNumber(int numero);
	int beginSeq(int numSeq);

	//Save / Restore
	void saveAll(string file);


	// Get / Set / Print
	void visuWt();
	void printLabels() const;

	int nbLettresRoot() const;
	int nbAnnoDiff() const;
	string getLabel(int i) const;
	int sizeAnno() const;
	int sizeLink() const;
	int sizeHelper() const;
	void printSeq() const;


};
#endif