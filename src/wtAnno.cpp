#include <iostream>

#include "wtAnno.hpp"
#include "visu.cpp"

using namespace std;
using namespace sdsl;


WTAnno::WTAnno(){
    map<uint16_t, pair<string, int>> f;
	this->freq = f;
	pair<string, int> p ("NOTAG", 0);
	uint16_t n= 65535;
	this->freq[n]=p;
	map<string, uint16_t> c;
	this->labelCode = c;
	this->labelCode["NOTAG"] = 65535;
}

WTAnno::WTAnno(string file) : Anno(file){
	string fileWt = file + ".wt";
    wt_vdj_int<rrr_vector<63>> wt1;
    load_from_file(wt1, fileWt);
	this->wt = wt1;

   	string fileMap = file + ".map";
   	restoreMap(fileMap);
}

WTAnno::~WTAnno(){
	this->freq.clear();
	this->labelCode.clear();
}


void WTAnno::ajoutTagInMap(ArbreVDJ *arb, string nomGene){
	uint16_t code;

	if(this->labelCode.count(nomGene)>0)
		code = getCodeFromName(nomGene);				
	else{
		code = arb->findCell(nomGene)->getCode();
		ajoutCodeinMap(nomGene, code);
	}

	if (this->freq.find(code) != this->freq.end())
		this->freq[code].second++;
	else{
		pair<string, int> p (nomGene, 1);
		this->freq[code]=p;
	}
}

void WTAnno::ajoutCodeinMap(string str, uint16_t val){
    this->labelCode[str]=val;
}


void WTAnno::makeWt(vector<uint16_t> racine){
	int taille = racine.size();
    int_vector<> v(taille, 65535); //remplacer par TAGNULL
    for (int i=0; i<taille; i++)
        v[i] = racine[i];

	construct_im(this->wt, v);
}

/********** Requests *********/

// //returns the label of the position pos
string WTAnno::getLabel(int pos){
	int p = posBwtToWt(pos);
	uint16_t code =this->wt[p];
	// cout << "position " << pos << "->" << p <<  ":" << code <<endl;
	return getNameFromCode(code);
}

// //returns the positions of the label l
vector<int> WTAnno::getPositions(string anno){
	uint16_t code = getCodeFromName(anno);
	// cout << "code " << code << endl;
	vector<int> res;
	//TODO: fonction à changer !!
	if (code != 65535){
		int occ = this->wt.rank(this->wt.size(), code);
		for (int i=1; i<occ+1; i++)
			res.push_back(this->wt.select(i, code));
	}
	vector<int> pos;
	vector<int> vlink;
	for (int i=0; i<res.size(); i++){
		vlink = posWtToBwt(res[i]);
		// cout << "vlink taille " << vlink.size() << endl; 
		for (int j=0; j<vlink.size(); j++)
			pos.push_back(vlink[j]);
	}

	return pos; 
}

vector<int> WTAnno::findMotifXAnno(string anno, vector<int> vmotif){
	uint16_t codeAnno = getCodeFromName(anno);
	int cpt=0;
	vector<int> pos;
	bool found = false;
	int r = posBwtToWt(vmotif[0]);
	if (this->wt[r] == codeAnno){
		found = true;
		pos.push_back(vmotif[0]);
	}

	int p;
	for (int i=1; i<vmotif.size(); i++){
		p = posBwtToWt(vmotif[i]);
		if (p == r){		//same position in WT's root
			if (found == true)	
				pos.push_back(vmotif[i]);
		}
		else{
			cpt++;
			if (this->wt[p] == codeAnno){
				pos.push_back(vmotif[i]);
				found = true;
				r = p;
			}
			else
				found = false;
		}
	}
	// cout << "span " << vmotif.size() << " racine WT " << cpt << endl;
	return pos;
}



//pos bwt -> pos racine wt : O(1) 
int WTAnno::posBwtToWt(int pos){
	assert(pos < this->taille);
	int p;		// p = nombre de 1 avant pos dans lien
	p = this->b_rank(pos);
	if (p== this->b_rank(pos+1))
		p --;
	return p;
}


//pos racine wt -> liste pos bwt : O(occ)
vector<int> WTAnno::posWtToBwt(int pos){
	vector<int> res;
	int un= this->b_sel(pos+1);
	int unSuivant = min(this->b_sel(pos+2), this->link.size()+1);
    while(un < unSuivant){
    	res.push_back(un);	
    	un++;
	}
    return res;
}

pair<string, int> WTAnno::getElementMap(int i) {
	map<uint16_t, pair<string, int>>::iterator it = this->freq.begin(); 
	int j=0;
	while(it != this->freq.end() && j<this->freq.size()){
	if (i==j)
		return it->second;
	j++;
	it++; 
	}
}

string WTAnno::getNameFromCode(uint16_t code){
	return this->freq[code].first;
}

uint16_t WTAnno::getCodeFromName(string name){
	uint16_t res = this->labelCode[name];
	if (getNameFromCode(res) != name)
		res = -1;
	return res;
}

int WTAnno::getFreq(string name){
	
     map<uint16_t, pair<string, int>>::iterator it = this->freq.begin(); 
    while(it != this->freq.end()){
        if (it->second.first == name)
        	return it->second.second;
        it++;
    }
    return -1;
}

int WTAnno::countMapLabel(string name) const{
	return this->labelCode.count(name);
}



/********** Save/Restore *********/


void WTAnno::saveAnno(string file){
	string fileWt = file + ".wt";
    store_to_file(wt, fileWt);
   	string fileMap = file +".map";
   	saveMap(fileMap);
	string fileLink = file + ".link";
	saveLink(fileLink);
}

void WTAnno::saveMap(string file){
    ofstream fluxEc(file.c_str());  
    if(fluxEc){
    	fluxEc << this->freq.size() << endl;
    	for (map<uint16_t, pair<string, int>>::iterator it=this->freq.begin(); it!=this->freq.end(); ++it){
    		fluxEc << it->first << " " << it->second.first << " " << it->second.second << endl;
    	}
    	fluxEc << this->labelCode.size() << endl;
    	for (map<string, uint16_t>::iterator it=this->labelCode.begin(); it!=this->labelCode.end(); ++it){
    		fluxEc << it->first << " " << it->second << endl;
    	}
	fluxEc.close();
	} else
        cerr << "Save Map : Error when trying to open file (writing) : " << file << endl;
}

void WTAnno::restoreMap(string file){
	ifstream fluxLec(file.c_str());
    if (fluxLec){
    	int size;
    	uint16_t code;
    	string name;
    	fluxLec >> size;
    	for (int i=0; i<size; i++){
    		fluxLec >> code;
   			pair<string, int> p;
    		fluxLec >> p.first;
    		fluxLec >> p.second;
    		this->freq[code] = p;
    	}

    	fluxLec >> size;
    	for (int i=0; i<size; i++){
    		fluxLec >> name;
    		fluxLec >> code;
    		this->labelCode[name] = code;
    	}

		fluxLec.close();
    } else
    	cerr << "Error when trying to open file (reading) :" << file << endl; 
}


/********** Get/Set/Print *********/

void WTAnno::visuWt()const{
	vector<string> vs(2, "");
	cout << "WT : " << endl;
	visualize_wt_rec(this->wt, this->wt.root(), 0, vs);
	for (size_t i=0; i<vs.size(); ++i)
        cout<<vs[i]<<endl;

    cout << "Map freq: " << endl;
    printLabels();

	cout << "Map labelCode: " << endl;
	for (map<string, uint16_t>::const_iterator it=this->labelCode.begin(); it!=this->labelCode.end(); ++it){
   		cout << it->first << " " << it->second << endl;
   	}
}

void WTAnno::printLabels()const {
	map<uint16_t, pair<string, int>>::const_iterator it = this->freq.begin(); 
	while(it != this->freq.end()){
		cout << it->second.first << " : " << it->second.second << endl;
	it++;
	}
	cout << endl;
}


int WTAnno::nbLettresRoot() const {
	return this->wt.size();
}

int WTAnno::sigma() const {
	return this->freq.size();
}

int WTAnno::sizeAnno() const{
	return size_in_bytes(this->wt);
}


int WTAnno::sizeMap() const{
	int size=this->labelCode.size()*sizeof(uint16_t);		//map labelCode
	for (map<string, uint16_t>::const_iterator it=this->labelCode.begin(); it!=this->labelCode.end(); ++it){
    	size += it->first.size();
	}	

	size += this->freq.size() * (sizeof(int) + sizeof(uint16_t));	//map freq
	for (map<uint16_t, pair<string, int>>::const_iterator it=this->freq.begin(); it!=this->freq.end(); ++it){
    	size += it->second.first.size();
	}	

    return size;
}
