#ifndef WTANNO_H
#define WTANNO_H


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

#include "anno.hpp" // parent class

#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <sdsl/wavelet_trees.hpp>


using namespace std;
using namespace sdsl;

class WTAnno : public Anno {

private:
	wt_vdj_int<rrr_vector<63>> wt;
	/* The map the number of occurrences and the code of every VDJ gene of the file	
	The number of occurrences is used to make the shape of the WT	*/
	map<uint16_t, pair<string, int>> freq;
	//map whoch made the translation between name(string) and code(uint16_t)
	map<string, uint16_t> labelCode;



public:
	WTAnno();
	WTAnno(string file);
	~WTAnno();
	void makeWt(vector<uint16_t> racine);

	void ajoutTagInMap(ArbreVDJ* arb, string nomGene);
	void ajoutCodeinMap(string str, uint16_t val);


	//returns the label of the position pos
	string getLabel(int pos);
	//returns the positions of the label l (do not returns anything with the label NOTAG)
	vector<int> getPositions(string anno);
	//returns the positions of the patterns which have the label anno
	vector<int> findMotifXAnno(string anno, vector<int> vmotif);
	int posBwtToWt(int pos);
	vector<int> posWtToBwt(int pos);
	pair<string, int> getElementMap(int i) ;
	string getNameFromCode(uint16_t code);
	uint16_t getCodeFromName(string name);
	int getFreq(string name);
	int countMapLabel(string name) const;

	//save 
	void saveAnno(string fileWt);
	void saveMap(string file);
	//restore 
	void restoreMap(string file);

	void visuWt()const;
	void printLabels() const;

	int nbLettresRoot() const;
	int sizeAnno() const;
	int sizeMap() const;
	int sigma() const;
};


#endif