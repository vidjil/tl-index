/*
  This file is part of tl-index
  Copyright (C) 2014-2017 by Bonsai bioinformatics
  at CRIStAL (UMR CNRS 9189, Université Lille) and Inria Lille
  Contributors: 
      Tatiana Rocher <tatiana.rocher@vidjil.org>

  "tl-index" is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  "tl-index" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with "tl-index". If not, see <http://www.gnu.org/licenses/>
*/


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <utility>
#include <ctime> 
#include <unistd.h>
#include <cstdlib>
#include <cstdio>  
#include <random>
#include <chrono>


#include <sdsl/io.hpp>
#include <sdsl/bit_vectors.hpp>

#include <random>

#include "index.hpp"
#include "wttindex.hpp"
#include "tlIndex.hpp"
#include "mapIndex.hpp"
#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include "outils.hpp"
#include "annotation.hpp"

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

/*installation : 
- 	installer SDSL : attention, cela va mettre deux dossier dans la racine : include et lib
	git clone git://github.com/simongog/sdsl-lite
	cd sdsl-lite
	./install.sh

-	make
- 	./main in.fa out.bwt

*/

/* Fichier Fasta-like utilisé : 
> liste des annotations de sequence1 : anno1:début-fin anno2... (début et fin compris dans l'annotation)
séquence1
> ...
*/


using namespace std;


void usage(){
	cout << "How to call the main : ./main -o fileIn.fa options." << endl;
	cout << "option -r to show les résultats intermédiaires." << endl;
	cout << "option -t to show a timer." << endl;	
	cout << "option -s to show some size statistics." << endl;
	cout << "option -e to show some examples of access to the data" << endl;
	cout << "option -v <nameFile> to save the index (the nameFile doen't have any extension)." << endl;
	cout << "option -l <nameFile> to restore the index (the nameFile doesn't have any extension)." << endl;
	cout << "option -d to have a WT which indexes the labels in the order of the original text" << endl;
	cout << "option -m to replace the WT with a Map" << endl;
}


int main(int argc, char* argv[]){
	
	if (argc < 2){
		usage();
		return 0;
	}

		char c;
		bool result = false;
		bool btime = false;
		bool stats = false;
		bool exemple = false;
		bool save = false;
		bool fileIn = false;
		bool order = false; //if true, use a WT which indexes the labels in the order of the original text
		bool map = false;	// if true, use a map instead of the WT
		string in;
		string saveFile;
		chrono::time_point<chrono::system_clock> start, end, debut, fin;
		chrono::duration<double> texec;
    	
		Index *b;

    	
		while ((c = getopt(argc, argv, "dehl:mo:srtv:")) != EOF){

 			switch (c) {
 				case 'h':
 					usage();
 					break;
 				case 'd':
 					order = true;
 					break;
 				case 'e':
 					exemple = true;
 					break;
 				case 'l':
 					saveFile = optarg;
 					b = new TLIndex(saveFile, result);
 					// b->restoreIndex(saveFile);
					// hp = restoreHelper(saveFile);
					cout << endl << "Restore done" << endl;
					break;
				case 'm' :
					map = true;
					break;
				case 'o' :
					in = optarg;
					fileIn = true;
					break;
				case 'r':
 				    result = true;
 				    break;
 				case 's':
 					stats = true;
 					break;
 				case 't':
 					btime = true;
 					break;
 				case 'v':
	 				save = true;
	 				saveFile = optarg;
	 				break;
 				default:
 					usage();
 					return 0;
			}
		}


		/********	Construction   *******/
		
		if (fileIn){
			string dossier(argv[0]);

			if (btime)
				start = chrono::system_clock::now();

			// btime and result are boolean which allows to print the structures and their construction time
			if (order)
				b = new WTTIndex(dossier, in, btime, result);	
			else if (map)
				b = new MapIndex(dossier, in, btime, result);
			else
				b = new TLIndex(dossier, in, btime, result);

			if (btime){
				end = chrono::system_clock::now();
				texec = end-start;
				cout << "Total time : " << texec.count() << "s" << endl;
			}			
					
		}

		/**************		Stats   *************/

		if (stats){
			cout << "Number of letters : " << b->nbLettres() << endl;
			cout << "Distributed as $ACGTN : " ;
			int64_t d[6];
			b->getRepartitionLettres(d);
			for (int i=0; i<6; i++)
				cout << d[i] << " " ;
			cout << endl;
			cout << "Number of letters in the WT's root : " << b->nbLettresRoot() << endl;
			cout << "Number of unique labels : " << b->nbAnnoDiff() << endl;
			cout << "Size of BWT : " << b->sizeBwt(in) << " bytes" << endl;
			cout << "Size of Link : " << b->sizeLink() << " bytes" << endl;
			cout << "Size of Label struct : " << b->sizeAnno() << " bytes" << endl;
			cout << "Size of secondary struct : " << b->sizeHelper() << " bytes" << endl;
			cout << "Total : " << (b->sizeBwt(in) + b->sizeLink() + b->sizeAnno() + b->sizeHelper()) << " bytes" << endl;
		}



		/***********	Sauvegarde/restauration   **********/

		if (save){
			b->saveAll(saveFile);
			cout << endl << "Backup done" << endl;
		}



		/************	Acces aux informations ***********/

		if (exemple){
			if (btime)
				start = chrono::system_clock::now();

			cout << endl;
			cout << "Example of access to the data : " << endl << endl; 

			cout << "------ Show the sequences: " << endl;
			cout << "0 : " << b->seq(0) << endl;
			cout << "1 : " << b->seq(1) << endl;
			cout << "2 : " << b->seq(2) << endl;
			cout << endl;

			if (b->nbLettres() < 30){
				cout << endl << "------ List of labels by position : " << endl;
				for (int i=0; i<b->nbLettres(); i++){
					//cout << i << endl;
					cout << b->label(i) << " ";
				}
				cout << endl;
			}

			int l = b->nbLettres();
			random_device rd;
    		mt19937 gen(rd());
    		uniform_int_distribution<> dis(0, l-1);			
			if (btime){
				debut=chrono::system_clock::now();
				for (int i=0; i<1000000; i++){
					int pos = dis(gen);
					b->label(pos);
				}
				fin=chrono::system_clock::now();
				texec=fin-debut;
				cout << endl << "Execution time for 1M label: " << texec.count() << " s." << endl;
				
				debut=chrono::system_clock::now();
				for (int i=0; i<10000000; i++){
					int pos = dis(gen);
					b->label(pos);
				}
				fin=chrono::system_clock::now();
				texec=fin-debut;
				cout << endl << "Execution time for 10M label: " << texec.count() << " s." << endl;
			}


			if (btime){
				debut=chrono::system_clock::now();		
				for (int i=0; i<100000; i++){
					int pos = dis(gen);
					b->numSeq(pos);
				}
				fin=chrono::system_clock::now();
				texec=fin-debut;
				cout << endl << "Execution time for 100000 numSeq: " << texec.count() << " s." << endl;
			}


			string anno = b->label(0);
			cout << endl << "------ First label : " << anno << endl;

			cout << endl << "------ Number of unique labels : " << b->nbAnnoDiff() << endl;
			if (b->nbAnnoDiff()<=5) {
				cout << endl <<"------ Number of occurrences of the labels : " << endl;
				b->printLabels();
			}

			vector<int> seq;
			cout << endl << "------Sequences which have the label :" << endl;
			if (btime)
				debut=chrono::system_clock::now();
			seq = b->findL("IGHV3-21*01");
			if (btime){
				fin=chrono::system_clock::now();
				texec=fin-debut;
				cout <<  "Execution time : " << texec.count() << endl;
			}
			cout << "IGHV3-21*01 is in " << seq.size() << " letters" << endl;
			// cout << " and " << b->numSeq(seq).size() << " sequences." << endl;
			
			if (btime)
				debut=chrono::system_clock::now();
			seq = b->findL("TRGJ1*02");
			if (btime){
				fin=chrono::system_clock::now();
				texec=fin-debut;
				cout <<endl << "Execution time : " << texec.count() << endl;
			}
			cout << "TRGJ1*02 is on " << seq.size() << " letters" << endl;
			// cout << " and " << b->numSeq(seq).size() << " sequences." << endl;
			
			if (btime)
				debut=chrono::system_clock::now();
			seq = b->findL("IGHJ5*02");
			if (btime){
				fin=chrono::system_clock::now();
				texec=fin-debut;
				cout <<endl << "Execution time : " << texec.count() << endl;
			}
			cout << "IGHJ5*02 is on " << seq.size() << " letters" << endl;
			// cout << " and " << b->numSeq(seq).size() << " sequences." << endl;
			
			// cout << endl << "------ Labels of the séquence 0 : " << endl;
			// vector<Annotation*> v;
			// v= b->findAnnoFromNumber(0); 
			// for (int i=0; i<v.size(); i++)
			// 	v[i]->toString();

			cout << endl << "------ List of the sequences which have a pattern " << endl;
			string motif = "AAC";
			if (btime)
				debut = chrono::system_clock::now();
			vector<int> w = b->findMotif(motif);
			if (btime){
				fin = chrono::system_clock::now();
				texec = fin-debut;
				cout << "Execution time : " << texec.count() << endl;
			}
			cout << motif <<  " present : " << w.size() << " times" << endl;
			// cout << " in " << b->numSeq(w).size() << " sequences." << endl;
			if (w.size() < 10){
				vector<int> w2 = b->numSeq(w);
				for (int i=0; i<w.size(); i++)
					cout << w2[i] << " " << b->seq(w2[i]) << endl;
			}

			cout << endl << "------Letters which start the pattern AAC and are labeled IGHJ5*02 : " ;
			if (btime)
				debut=chrono::system_clock::now();
			seq =b->findMotifXAnno("TCT", "TRGJ1*02");
			if (btime){
				fin=chrono::system_clock::now();
				texec=fin-debut;
				cout << "Execution time : " << texec.count() << endl;
			}
			cout <<"(number : " << seq.size() << ")" << endl;

			if (seq.size()<=10){
				for (int i=0; i<seq.size(); i++)
					cout << b->numSeq(seq[i]) << " " << b->seq(b->numSeq(seq[i])) << endl;
				cout << endl;
			}

			if (btime){
				end = chrono::system_clock::now();
				texec = end-start;
				cout << endl << "Total execution time of the queries: " << texec.count() << endl;
			}
			


		}

		/*******************************************/
	

		
		delete b;
		

	
} 
/**/
