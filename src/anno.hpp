#ifndef ANNO_H
#define ANNO_H


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <sdsl/wavelet_trees.hpp>


using namespace std;
using namespace sdsl;

class Anno{
protected:
	rrr_vector<127> link;
	rrr_vector<127>::rank_1_type b_rank;
	rrr_vector<127>::select_1_type b_sel;

public:
	Anno();
	Anno(string file);
	~Anno();
	void setLink(bit_vector l);

	//queries
	virtual string getLabel(int pos) = 0;
	virtual vector<int> getPositions(string anno) = 0;
	virtual int posBwtToWt(int pos) = 0;
	virtual vector<int> posWtToBwt(int pos) = 0;

	//save
	void saveLink(string fileLink);
	//restore
	void restoreLink(string file);

	//print / get / set
	virtual void printLabels() const = 0;
	void printLink() const;
	virtual int sigma() const = 0;
	int sizeLink() const;
};


#endif