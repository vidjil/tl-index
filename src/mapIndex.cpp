#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <tuple>
#include <cassert>
#include <map>
#include <vector>
#include <bitset>
#include <cstdint>

#include "mapIndex.hpp"


// uint16_t TAGNULL = 65535; // max : 1111111111111111

using namespace std;
using namespace sdsl;


/****************
  Construction
****************/


MapIndex::MapIndex(string file, bool result) : Index(file, result){
	cout << "Restoration ignored" << endl;
}

MapIndex::MapIndex(string dossier, string fileIn, bool btime, bool result) : Index(dossier, fileIn, btime, result){
	time_t tend, tmid;
    double texec=0.;	
	tmid=time(NULL);

	MapAnno *h = new MapAnno(fileIn, this->taille);
	this->ht = h;
	bit_vector link = bit_vector(taille, 0);
	bit_vector s = bit_vector(taille, 0);

	
	ifstream in(fileIn);
	string strBegin, strEnd, name;
	int begin, end;
	string line;
	int totalL = 0;
	int cptSeq=0;
	int tmpEnd= -1;
	int tmpEndSeq=0; // nombre de lettres avant la dernière séquence

	if (in){
	//lecture ligne
		while(getline(in, line)){
			s[totalL] = 1;
			int i=1; // après '>'
			int j=1;
			end = -1;
			tmpEnd = -1;
			while(i<line.size()){
				while (line[i]!=':' && i<line.size()){
					i++;
				}
				if (i == j) break;
				name.assign(line.begin()+j, line.begin()+i);
				j=i+1;
				
				while(line[i]!='-'&& i<line.size()){
					i++;
				}
				if (i == j) break;
				strBegin.assign(line.begin()+j, line.begin()+i);
				begin = stoi(strBegin);
				j=i+1;
				
				while(line[i]!=' ' && i<line.size()){
					i++;
				}
				if (i == j) break;
				strEnd.assign(line.begin()+j, line.begin()+i);
				end = stoi(strEnd);
				j=i+1;
			


 				//add in m and in Link
				if (tmpEnd +1 <begin){
					this->ht->addInM("NOTAG");
					this->ht->addAnno("NOTAG", cptSeq, tmpEnd+1, begin-1);
					link[tmpEnd+1+totalL] = 1;
					// for (int k =tmpEnd+2; k<begin; k++)
						// link[k]=0;
				}
				this->ht->addAnno(name, cptSeq, begin, end);
				this->ht->addInM(name);
				link[begin+totalL] = 1;
				// for (int k =begin+ totalL+1; k<=end+totalL; k++)
				// 	link[k]=0;

				tmpEnd = end;
				}
 			getline(in, line); // line ADN
 			tmpEndSeq = totalL;
 			totalL += line.size()+1;
 			cptSeq++;
 			if (tmpEnd +1 <totalL){
				this->ht->addInM("NOTAG");
				link[tmpEnd + tmpEndSeq + 1] = 1;
				// for (int k =tmpEnd+tmpEndSeq+ 2; k<totalL; k++)
				// 	link[k]=0;
				this->ht->addAnno("NOTAG", cptSeq, tmpEnd+2, line.size()-1);
 			}
 		}
	in.close();
	} else
		cerr << "Error when trying to open the file " << endl;


	if (btime){	
		cout << "Map 		->	OK" ;
		tend=time(NULL);
		texec=difftime(tend,tmid);
		cout << "	Done in : " << texec << "s" << endl;
	}
	this->ht->setLink(link);
	setSeq(s);
	if (result){
		this->ht->visuHt() ;
		cout << "Seq " << this->seq << endl;
	}
}

MapIndex::~MapIndex(){
	// ht->clear();
}


void MapIndex::setSeq(bit_vector seq){
	rrr_vector<127> rrrs(seq);
	this->seq = rrrs;
	rrr_vector<127>::rank_1_type r(&this->seq);
	this->s_rank = r;
	rrr_vector<127>::select_1_type s(&this->seq);
	this->s_sel = s;
}

/***************************
		Searches

Abbreviations of the complexities : 
	-rank2a : complexity of rank2a : lengthRope + sizeLeafRope
	-select : same

***************************/

//pos bwt 	-> pos text : O(log(n)^(1+epsilon)) 
//pos text 	->	pos root wt : O(1) 	\
//pos root 	-> leaf wt : O(w)		|>	O(log(n)^(1+epsilon) + w)	
//leaf		-> label : O(1)			/
string MapIndex::labelU(int pos){
	pair<int, int> p = this->b->findNumeroSeqFromPos(pos);
	int pp = beginSeq(p.first+1) - p.second;
	return this->ht->getLabel(pp);
}

//pos bwt -> nom annotation : O(rechercheMap) 
string MapIndex::label(int pos){
	// uint16_t code = findAnnoByPos(pos);
	// return hp->getNameFromCode(code);
	return this->ht->getLabel(pos);
}


//pattern -> range of occurrences of the pattern : O(pattern*lengthRope)
// theorique : O(motif)
vector<int> MapIndex::findMotif(string motif){
	int debut=0;
	int fin= this->taille;
	this->b->findMotif(motif, &debut, &fin);
	vector<int> res;
	int pos;
	for (int i=debut; i<fin; i++){
		pos = b->previousLetter(i);
		res.push_back(posBwtToWt(pos));
	}
	return res;
}

// annotation -> liste n° seq which have anno : O(occ1 + occ2*read)
// occ1 : number of occurences found at the root of the WT
// occ2 : number of occurences in the BWT (occ2 >= occ1)
vector<int> MapIndex::findL(string anno){
	vector<pair<int, int>> v = this->ht->getPositionsMap(anno);
	return getPositions(v);
}



//pattern + anno ->> list if the se's id which have the pattern which have the anno : 
// real : O(motif*hauteurRope + occ1*w +occ2*read)
// theorique : O(motif+ occ1*w +occ2*read)

vector<int> MapIndex::findMotifXAnno(string motif, string anno){
	vector<int> vmotif = findMotif(motif);
	vector<int> lettres;

	for (int i=0; i<vmotif.size(); i++){
		if (label(vmotif[i]) == anno)
			lettres.push_back(vmotif[i]);
	}
	return lettres;
}




/********************************
	Accessors to the sequences
********************************/


//pos bwt -> n° sequence : O(select)
int MapIndex::numSeq(int pos){
	assert(pos < this->taille);
	int p = this->s_rank(pos);
	if (p == this->s_rank(pos+1))
		p--;
	return p;
}

//liste pos bwt -> liste n° seq : O(pos*select)
vector<int> MapIndex::numSeq(vector<int> pos){
	vector<int> res;
	int seq;
	bool find = false;
	for (int i=0; i<pos.size(); i++){
		seq = numSeq(pos[i]);
		for (int j=0; j<res.size(); j++)
			if (res[j] == seq){
				find = true;
				break;
				}
		if (!find)	
			res.push_back(seq);
		find = false;
	}
	return res;
}


int MapIndex::beginSeq(int numSeq){
	assert(numSeq <= this->s_rank(this->taille));
	int pos = this->s_sel(numSeq+1);
	if (pos > this->seq.size())
		pos = this->seq.size();
	return pos;
}

int MapIndex::getPosition(pair<int, int> p){
	return (beginSeq(p.first) + p.second);
}

vector<int> MapIndex::getPositions(vector<pair<int, int>> duo){
	vector<int> u;
	for (int i=0; i<duo.size(); i++)
		u.push_back(getPosition(duo[i]));
	return u;
}


vector<Annotation*> MapIndex::findAnnoFromNumber(int numero){
;
}


int MapIndex::posBwtToWt(int pos){
	pair<int, int> p = this->b->findNumeroSeqFromPos(pos);
	return beginSeq(p.first+1) - p.second;
}


vector<int> MapIndex::posBwtToWt(vector<int> motif){
	vector<int> v;
	pair<int, int> p;
	int pos;
	for (int i=0; i<motif.size(); i++){
		p = this->b->findNumeroSeqFromPos(motif[i]);
		pos = beginSeq(p.first+1) - p.second;
		v.push_back(pos);
	}
	return v;
}


/******************************
	Store/Restore
******************************/

void MapIndex::saveAll(string file){
	cout << "saving ignored " << endl;
}


/***************************
		Visualisation
***************************/

void MapIndex::visuWt(){
	this->ht->visuHt();
}


/***********************
	Getter/Setter
***********************/

int MapIndex::nbLettresRoot() const{
	return 0;
}

int MapIndex::nbAnnoDiff() const {
	return this->ht->sigma();
}

string MapIndex::getLabel(int i) const{
	return this->ht->getLabel(i);
}

int MapIndex::sizeAnno() const{
	return this->ht->sizeHt();
}

int MapIndex::sizeLink() const{
	return this->ht->sizeLink();
}

int MapIndex::sizeHelper() const{
	return size_in_bytes(this->seq) + size_in_bytes(this->s_rank) + size_in_bytes(this->s_sel);
}

void MapIndex::printLabels() const{
	this->ht->printHt();
}

