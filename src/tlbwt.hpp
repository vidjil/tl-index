#ifndef BWT_H
#define BWT_H

#include <iostream>
#include <string>
#include <vector>

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

using namespace std;

class BWT{

	private:
		int taille;
		mrope_t *mr;
		vector<int> c;


	public:
	BWT(string file);
	BWT();
	~BWT();

	//look for the position of the following letter in the BWT
	int followingLetter(int pos);
	// inverse as previous
	int previousLetter(int pos);

	//return the position of the letter which have been added between 2 consecutives positions
	// represented as 2 tables
	int extractionLettre (int64_t a[6], int64_t b[6]);

	//return the n° of the seq and the position of pos in the seq
	pair<int, int> findNumeroSeqFromPos(int pos);
	int findPosFromNumSeq(int seq, int pas);
	char findLettreBwt(int pos);
	// returns the xth sequence of the BWT
	vector<int> findSeqByNumber(int number);
	void findMotif(string motif, int *debut, int *fin);

	void saveBwt(string file);
	void restoreBwt(string file);

	void visuBwt();	
	int getTaille();
	mrope_t* getRope() const;
	vector<int> getTableC() const;
	void printC() const;
	void getRepartitionLettres(int64_t tab[6]);
	int sizeBwt(string file) const;
	int getC(int i);
	string seq(int i);

};
#endif