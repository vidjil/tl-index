#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <tuple>
#include <cassert>
#include <map>
#include <vector>
#include <bitset>
#include <cstdint>
#include <ctime> 

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include "outils.hpp"
#include "wtAnno.hpp"
#include "visu.cpp"
#include "tlbwt.hpp"
#include "index.hpp"

uint16_t TAGNULL = 65535; // max : 1111111111111111

using namespace std;
using namespace sdsl;


Index::Index(string file, bool result){
	BWT *b1 = new BWT(file);
	this->b = b1;
	this->taille = this->b->getTaille();

	if (result){
		b->visuBwt();
	}
}


Index::Index(){
	BWT *bwt;
}


Index::Index(string dossier, string file, bool btime, bool result){
	time_t tbegin,tend, tmid;
    double tbwt=0.;
    tbegin=time(NULL);

	BWT *bwt = new BWT(file);
	this->b = bwt;
	if (btime){
		cout << "BWT 		->	OK";
		tmid=time(NULL);
		tbwt=difftime(tmid,tbegin);
		cout << "	Done in "  << tbwt << "s" << endl;
	}
	if (result){
		b->visuBwt();
	}

	this->taille = this->b->getTaille();
}


int Index::vectorTag(vector<Annotation*> &vec, ArbreVDJ* arb, string ligne){

	string nomGene = "", debut = "", fin = "";
	int debutI, finI;
	int i=1; // on commence après le char '>' 
	int size=0;

	while(i < ligne.length()){
		while(ligne[i]!=':'){
			nomGene += ligne[i];
			i++;
		}
		i++;
		while(isdigit(ligne[i])){
			debut += ligne[i];
			i++;
		}
		i++;
		while(isdigit(ligne[i])){
			fin += ligne[i];
			i++;
		}
		i++;
		debutI = stoi(debut);
		finI = stoi(fin);
		assert((debutI>=0) && (debutI <ligne.length()));
		assert((finI>=0) && (finI <ligne.length()));
		assert (debutI < finI);

		Annotation *triplet = new Annotation(nomGene, debutI, finI);
		vec.push_back(triplet);

		nomGene.clear();
		debut.clear();
		fin.clear();
		size++;
	}
	return size;
}

/**************
	Queries
**************/


/********************************
	Accessors to the sequences
********************************/

//pos bwt -> lettre : O(rank2a) 
//theorique : O(1)
char Index::letter(int pos){
	return b->findLettreBwt(pos);
}


//liste pos bwt -> liste n° seq : O(pos*select)
vector<int> Index::numSeq(vector<int> pos){
	vector<int> res;
	int seq;
	bool find = false;
	for (int i=0; i<pos.size(); i++){
		seq = numSeq(pos[i]);
		// cout << "position " << pos[i] << " seq " << seq << endl;
		for (int j=0; j<res.size(); j++)
			if (res[j] == seq){
				find = true;
				break;
				}
		if (!find)	
			res.push_back(seq);
		find = false;
	}
	return res;
}


// n° sequence -> sequence : O(rank2a*longueurRead)
vector<int> Index::findSeqByNumber(int number){
	return this->b->findSeqByNumber(number);
}

/***********************
	Getter/Setter
***********************/


void Index::visuBwt(){
	// mr_print_tree(this->mr);
	this->b->visuBwt();
}

void Index::printC() const{
	this->b->printC();
}

mrope_t* Index::getRope() const{
	// return this->mr;
	this->b->getRope();
}

vector<int> Index::getTableC() const{
	// return this->c;
	return this->b->getTableC();
}

int Index::nbLettres() const{
	return this->taille;
}

void Index::getRepartitionLettres(int64_t tab[6]){
	// mr_get_c(this->mr, tab);
	this->b->getRepartitionLettres(tab);
}

int Index::sizeBwt(string file) const{
	return this->b->sizeBwt(file);
}

string Index::seq(int i){
	return b->seq(i);
}


