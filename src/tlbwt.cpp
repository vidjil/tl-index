#include <iostream>
#include <string>
#include <algorithm>

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

#include "outils.hpp"
#include "tlbwt.hpp"


using namespace std;

BWT::BWT(string file){
	mrope_t *rope = bwt(file.c_str());
	this->mr = rope;
	int64_t d[7];
	mr_get_ac(mr, d);
	vector<int> c;
	for (int i=0; i<7; i++)
		c.push_back((int) d[i]);
	this->c = c;
	this->taille = d[6];
}

// construction from a saved file
BWT::BWT(){
}

BWT::~BWT(){
	mr_destroy(mr);
	this->c.clear();


}

int BWT::followingLetter(int pos){
	int64_t ok[6], ol[6];
	mr_rank2a(this->mr, pos, pos+1, ok, ol);

	int lettreInt = extractionLettre(ok, ol);
	char lettre = IntToADN(lettreInt);
	pos = ok[lettreInt] + this->c[lettreInt];
	return pos;
}

int BWT::previousLetter(int pos){
	int64_t prev;
	int lettreInt=0;
	while((pos-this->c[lettreInt+1] >= 0) /*|| (lettreInt <6)*/){
		lettreInt++;
		}

	pos -= c[lettreInt]; 

	mr_select(this->mr, pos+1, lettreInt, &prev);
	pos = prev-1;

	char lettre = IntToADN(lettreInt);
	return pos;
}

int BWT::extractionLettre(int64_t a[6], int64_t b[6]){
	for (int i=0; i<6; i++)
		if (a[i] != b[i])
			return i;
	int res = 7;
	return res;
}

char BWT::findLettreBwt(int pos){
	int64_t ok[6], ol[6];
	mr_rank2a(this->mr, pos, pos+1, ok, ol);

	int lettreInt = extractionLettre(ok, ol);
	char lettre = IntToADN(lettreInt);
	return lettre;
}

//pos bwt -> n° sequence : O(select)
pair<int, int> BWT::findNumeroSeqFromPos(int pos){
	int seq=pos;
	int nbSeq = this->c[1];
	int i=0;
	while(seq >= nbSeq){
		seq = previousLetter(seq);
		i++;
	}
	pair<int, int> p(seq, i);
	return p;
}

//n° seq + id	-> pos bwt
int BWT::findPosFromNumSeq(int seq, int pas){
	assert(seq < this->c[1]);
	int pos = seq;
	for (int i=0; i<pas; i++){
		pos = followingLetter(pos);
	}
	return pos;
}

// n° sequence -> sequence : O(rank2a*longueurRead)
vector<int> BWT::findSeqByNumber(int number){
	vector<int> res;
	// string sequence = "";
	if (number < 0){
		cerr << "This sequence does not exists" << endl;
		return res;
	}
	else if(number >=this->c[1]){
		cerr << "This id number is greater than the number of sequences." << endl;
		return res;
	}
	else{
		int pos = number;
		int64_t ok[6], ol[6];
		int letterPos;
		char letter =' ';
		while(letter != '$'){
			res.push_back(pos);
			mr_rank2a(mr, pos, pos+1, ok, ol);

			letterPos = extractionLettre(ok, ol);
			letter = IntToADN(letterPos);
			pos = ok[letterPos] + c[letterPos];
		}
		res.pop_back();
		return res;
	}
}

void BWT::findMotif(string motif, int *debut, int *fin){
	if (motif.size() == 0)
		return;
	char l = motif[motif.size()-1];
	int lettreInt = ADNToInt(l);
	if (lettreInt == 6){
		cout << "The pattern has an incorrect letter (not in ATGCN) : " << l << endl;
		return;
	}
	int64_t ok[6], ol[6];
	mr_rank2a(this->mr, *debut, *fin, ok, ol);
	*debut = this->c[lettreInt] + ok[lettreInt];
	*fin = this->c[lettreInt] + ol[lettreInt];

	motif.pop_back();	//delete derniere lettre
	findMotif(motif, debut, fin);
}


/************* Save / Restore *************/

void BWT::saveBwt(string file){
	// change in char*
	string fileRope = file + ".rope";
	char *cstr = new char[fileRope.length() + 1];
	strcpy(cstr, fileRope.c_str());
	saveRope(cstr, this->mr);
	delete [] cstr;
}

void BWT::restoreBwt(string file){
	string fileRope = file + ".rope";
	char *fRope = new char[fileRope.length() + 1];
	strcpy(fRope, fileRope.c_str());

	mrope_t *mr1 = restoreRope(fRope);
	this->mr = mr1;

	int64_t d[7];
	mr_get_ac(mr, d);
	vector<int> c;
	for (int i=0; i<7; i++)
		c.push_back((int) d[i]);
	this->c = c;
	this->taille = d[6];
}

/************* Set / Get / Print *************/

int BWT::getTaille(){
	return this->taille;
}

mrope_t* BWT::getRope() const{
	return this->mr;
}

vector<int> BWT::getTableC() const{
	return this->c;
}

void BWT::printC() const{
	cerr << "tableC : [" << this->c[0]<< ", " << this->c[1] << ", " << this->c[2] << ", "
		<< this->c[3]<< ", " << this->c[4] << ", " << this->c[5] << "]" << endl;
}

void BWT::visuBwt(){
	mr_print_tree(this->mr);
}

void BWT::getRepartitionLettres(int64_t tab[6]){
	mr_get_c(this->mr, tab);
}

int BWT::sizeBwt(string file) const{
	file.erase(file.end()-3, file.end());
	string fileRope = file + ".rope";
	char *cstr = new char[fileRope.length() + 1];
	strcpy(cstr, fileRope.c_str());
	int t = getSizeRope(cstr, this->mr);
	delete [] cstr;
	return t;
}

int BWT::getC(int i){
	return this->c[i];
}

string BWT::seq(int i){
	vector<int> pos = findSeqByNumber(i);
	string sequence = "";
	for (int i=0; i<pos.size(); i++){
		sequence += findLettreBwt(pos[i]);
	}
	reverse(sequence.begin(), sequence.end());
	return sequence;
}