#include <iostream>

using namespace std;

	// get the number corresponding to the letter :
	// $:0 A:1 C:2 G:3 T:4 N:5 other:6
int ADNToInt(char l){ 
	switch (l){ 
		case '$':
			return 0;
			break;
		case 'A':
			return 1;
			break;
		case 'C':
			return 2;
			break;
		case 'G':
			return 3;
			break;
		case 'T': 
			return 4;
			break;
		case 'N':
			return 5;
			break;
		default:
			cerr << "ADNToInt : la lettre lue ne fait pas parti de $ACGTN" << endl;
			return 6;
			break;
	}
}

	// inverse of previous function
char IntToADN(int i){ 
	switch (i){ 
		case 0:
			return '$';
			break;
		case 1:
			return 'A';
			break;
		case 2:
			return 'C';
			break;
		case 3:
			return 'G';
			break;
		case 4: 
			return 'T';
			break;
		case 5:
			return 'N';
			break;
		default:
			cerr << "IntToADN : la lettre lue ne fait pas parti de $ACGTN" << endl;
			return 'X';
			break;
	}
}
