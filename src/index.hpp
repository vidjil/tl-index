#ifndef INDEX_H
#define INDEX_H


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sdsl/bit_vectors.hpp>
#include <utility>
#include <sdsl/wavelet_trees.hpp>

#include "./ArbreVDJ/arbreVDJ.hpp"
#include "./ArbreVDJ/cell.hpp"
#include "annotation.hpp"
#include "wtAnno.hpp"
#include "tlbwt.hpp"
#include "tlbwt.hpp"

extern "C"{
#include "bwt.h"
#include "./RopeBwt2/mrope.h"
}

using namespace std;
using namespace sdsl;

class Index{

protected:
	BWT *b;
	int taille;


public :
	Index();
	//restore from file
	Index(string file, bool result);
	Index(string dossier, string file, bool btime, bool result);
	

	/* construction of the vector of labels
	fill the map of label frequencies */
	int vectorTag(vector<Annotation*> &vec, ArbreVDJ* arb, string ligne);

	//Queries	
	// returns the labels of a letter (in the order of the BWT, used in the TL-index)
	virtual string labelU(int pos) = 0;
	// returns the labels of a letter (in the order of the text T, used in the WT-index)
	virtual string label(int pos) = 0;
	// gives the begining and end of the area wich have the pattern "motif"
	virtual vector<int> findMotif(string motif) = 0;
	// returns the ids of the sequences which have the label anno
	virtual vector<int> findL(string anno) = 0;
	//returns the positions which have a pattern labelled anno
	virtual vector<int> findMotifXAnno(string motif, string anno) = 0;

	//Access
	// returns the letter at position pos int the BWT
	char letter(int pos);
	// from a position in the BWT, returns the sequence's id
	virtual int numSeq(int pos) = 0;
	// from positions of the BWT, gives the ids of the sequences
	vector<int> numSeq(vector<int> pos);
	vector<int> findSeqByNumber(int number);
	virtual vector<Annotation*> findAnnoFromNumber(int numero) = 0;
	virtual vector<int> posBwtToWt(vector<int> motif) = 0;

	//Save / Restore
	virtual void saveAll(string file) = 0;

	//Getters / Setters / Print
	void visuBwt();	
	void printC() const;
	string seq(int i);
	mrope_t* getRope() const;
	vector<int> getTableC() const;
	virtual string getLabel(int i) const = 0;
	int nbLettres() const;
	virtual int nbLettresRoot() const = 0;
	void getRepartitionLettres(int64_t tab[6]);
	virtual int nbAnnoDiff() const = 0;
	int sizeBwt(string file) const;
	virtual int sizeAnno() const = 0;
	virtual int sizeLink() const = 0;
	//Helper is all the map or bit vector which helper making a conversion (between positions, between code/string, ...)
	virtual int sizeHelper() const = 0;	
	virtual void printLabels() const = 0;

};
#endif