TL-index

clone the SDSL library and the TL-index in 2 repertories: 

clone SDSL:
$ git clone git@gitlab.inria.fr:vidjil/sdsl-lite.git

clone TL-index:
$ git clone git@gitlab.inria.fr:vidjil/tl-index.git

install the SDSL library : this can take up to 1 minute
$ cd sdsl-lite
$ ./install.sh
$ cd ..


install the TL-index :
$ cd tl-index/germline
$ sh get-saved-germline
$ cd ../src
$ make
Stay in this directory to run the programm.

run (you have to be in the src directory):
$ ./main -h  // to see list of options
$ ./main -o ./data/inGerm.fa -t


The index takes as an input file a fasta file of the following shape:
>nameLabel:begin-end otherLabel:begin-end
DNAsequence

example: 
>TRAV1-1*01:0-2 TRAJ1*01:3-5
AACAGC

From a Vidjil analyse: take the file.vidjil
in vidjil:
$ cd tools
$ python vidjil-to-fasta.py -o output.fa file.vidjil

in TL-index:
$ cd src/Autre
$ g++ faToFaBwtwt.fa -o faToFaBwtwt
$ ./faToFaBwtwt input.fa output.fa