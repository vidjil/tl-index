#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Outils
#include <boost/test/unit_test.hpp>
#include "../outils.cpp"

//g++ -o testOutils testOutils.cpp -lboost_unit_test_framework
 
BOOST_AUTO_TEST_CASE(ADN_vers_Int){
    BOOST_CHECK_EQUAL(ADNToInt('$'), 0);
    BOOST_CHECK_EQUAL(ADNToInt('A'), 1);
    BOOST_CHECK_EQUAL(ADNToInt('C'), 2);
    BOOST_CHECK_EQUAL(ADNToInt('G'), 3);
    BOOST_CHECK_EQUAL(ADNToInt('T'), 4);
    BOOST_CHECK_EQUAL(ADNToInt('N'), 5);
    BOOST_CHECK_EQUAL(ADNToInt('B'), 6);
    BOOST_CHECK_EQUAL(ADNToInt('Y'), 6);
    BOOST_CHECK_EQUAL(ADNToInt('D'), 6);
}

BOOST_AUTO_TEST_CASE(Int_vers_ADN){
    BOOST_CHECK_EQUAL(IntToADN(0), '$');
    BOOST_CHECK_EQUAL(IntToADN(1), 'A');
    BOOST_CHECK_EQUAL(IntToADN(2), 'C');
    BOOST_CHECK_EQUAL(IntToADN(3), 'G');
    BOOST_CHECK_EQUAL(IntToADN(4), 'T');
    BOOST_CHECK_EQUAL(IntToADN(5), 'N');
    BOOST_CHECK_EQUAL(IntToADN(6), 'X');
    BOOST_CHECK_EQUAL(IntToADN(10), 'X');
    BOOST_CHECK_EQUAL(IntToADN(42), 'X');
}