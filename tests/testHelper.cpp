#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Helper
#include <boost/test/unit_test.hpp>
#include "../helper.cpp"
#include "../ArbreVDJ/arbreVDJ.hpp"
#include "../ArbreVDJ/cell.hpp"

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <utility>
#include <tuple>
#include <cassert>
#include <map>
#include <vector>
#include <cstdint>

//g++ -std=c++11 -o testHelper testHelper.cpp ../ArbreVDJ/arbreVDJ.cpp ../ArbreVDJ/cell.cpp -lboost_unit_test_framework && ./testHelper

using namespace boost::unit_test;
using namespace std;

BOOST_AUTO_TEST_CASE(Helper_sizes){
    Helper *hp = new Helper();
    ArbreVDJ *arb = new ArbreVDJ();
    arb->chargerGermline("../ArbreVDJ/Germlines/TRAV.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRAJ.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRBV.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRBJ.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRBD.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRBD_upstream.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRDV.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRDJ.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRDD.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRDD2_upstream.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRDD3_downstream.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRDD_upstream.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/TRGV.fa"); 
    arb->chargerGermline("../ArbreVDJ/Germlines/TRGJ.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGHV.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGHJ.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGHD.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGHD_upstream.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGKV.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGKJ.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGK-KDE.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGK-INTRON.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGLV.fa");
    arb->chargerGermline("../ArbreVDJ/Germlines/IGLJ.fa");/**/

    //val de NOTAG
    BOOST_CHECK_EQUAL(hp->getFreq("NOTAG"), 0);
    BOOST_CHECK_EQUAL(hp->getCodeFromName("NOTAG"), 65535);
    BOOST_CHECK_EQUAL(hp->getNameFromCode(65535), "NOTAG");

    //taille map
    BOOST_CHECK_EQUAL(hp->sizeMap(), 1);
    //ajout et verif de differents tags
    hp->ajoutTagInMap(arb, "TRBJ2-2*01");
    hp->ajoutTagInMap(arb, "TRBJ2-7*02");
    BOOST_CHECK_EQUAL(hp->getFreq("TRBJ2-2*01"), 1);
    hp->ajoutTagInMap(arb, "TRBJ2-2*01");
    BOOST_CHECK_EQUAL(hp->sizeMap(), 3);
    //lorsque tag déjà présent : freq+=1
    BOOST_CHECK_EQUAL(hp->getFreq("TRBJ2-2*01"), 2);


    hp->resize(10);
    BOOST_CHECK_EQUAL(hp->sizeTags(), 10);
    for (int i=0; i<10; i++){
        hp->setTag(i, i);
        BOOST_CHECK_EQUAL(hp->getTag(i), i);
    }
    hp->cleanTag();
    BOOST_CHECK_EQUAL(hp->sizeTags(), 0);


}
